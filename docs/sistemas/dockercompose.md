# docker compose

Para poder definir mejor nuestro contenedor, su nombre, la imágen, las redes, las variables, etc... sin tener que crearnos *scripts* a partir de los comandos que vimos en el anterior apartado, tenemos el compose. 

La versión 1 era un aplicativo a parte, normalmente instalado con ```pip3 install docker-compose```. Actualmente las nuevas distribuciones ya vienen con el *docker v2* que ha integrado el compose y se puede ejecutar con ```docker compose```.

Referencia: [https://docs.docker.com/compose/](https://docs.docker.com/compose/)

Para realizar los siguientes ejercicios vamos a partir de la imágen *pythonserver* que hemos creado anteriormente, que servirá los ficheros desde el puerto *8000* por defecto.

El compose nos permite definir en un fichero *yml* las características de nuestro contenedor. Tendrá principalmente el apartado de *services*, en donde definiremos los contenedores, y el apartado *networks* opcional para definir las redes entre contenedores. Este seria nuestro *docker-compose.yml* (por defecto compose buscará ese fichero):

```
version: "3.7"
services:
  pythonserver1:
    image: pythonserver:latest
    build:
      context: .
      dockerfile: Dockerfile
    container_name: pythonserver1
    environment:
      MIVARIABLE: server1
    volumes:
    - type: bind
      source: ./output
      target: /output
      bind:
        create_host_path: true
    ports:
    - mode: ingress
      target: 8000
      published: "8000"
      protocol: tcp
    networks:
      red_miubuntu:
        ipv4_address: 172.31.255.10
    logging:
      driver: json-file
      options:
        max-size: 100m
  pythonserver2:
    image: pythonserver:latest
    build:
      context: .
      dockerfile: Dockerfile
    container_name: pythonserver2
    environment:
      MIVARIABLE: server2
    volumes:
    - type: bind
      source: ./output
      target: /output
      bind:
        create_host_path: true
    ports:
    - mode: ingress
      target: 8000
      published: "8081"
      protocol: tcp
    networks:
      red_miubuntu:
        ipv4_address: 172.31.255.20
    logging:
      driver: json-file
      options:
        max-size: 100m
networks:
  red_miubuntu:
    name: red_miubuntu
    driver: bridge
    ipam:
      config:
      - subnet: 172.31.255.0/24
```

Sobre las redes en docker. Por defecto las redes bridge tienen un gateway/dns por defecto establecido por el servicio dnsmasq de docker. Para más información sobre las redes en compose [https://docs.docker.com/compose/networking/](https://docs.docker.com/compose/networking/).

Ahora podremos realizar operaciones similares a las que hicimos con *docker* pero con el *docker compose*:

```
docker compose build
docker compose up -d
docker compose ps
docker compose logs
docker compose down
```

Con los contenedores iniciados deberíamos poder acceder a su servidor web:

```
curl http://172.31.255.10:8000
curl http://172.31.255.20:8081
```

Docker crea un conjunto de *iptables* para gestionar las redes dentro de los dockers. Así puede crear un *NAT* para abrir los puertos en el servidor y redirigirlos dentro de cada contenedor.

Podremos ver nuestra red *red_ubuntu* junto con otras por defecto que crea *docker* con el comando ```docker network ls```

Mientras que los puertos abiertos son el *8000* y el *8081* en el servidor, dentro de los contenedores será el 8000, ya que se han ejecutado a partir de la misma imagen los dos. Si entramos dentro del contenedor que se está ejecutando pythonserver1 usando las opciones de *terminal* e *interactive* podremos ejecutar comandos dentro y comprobar el puerto del *pythonserver2*:

```
docker exec -ti pythonserver1 /bin/sh
curl https://172.31.255.20:8000
```

Si miramos de nuevo en nuestra carpeta veremos que se ha creado un directorio 'output'. Este directorio es el *volumen* de datos del servidor que se ha *mapeado* dentro del *namespace* del contenedor. Múltiples contenedores pueden compartir el mismo volumen. Podemos comprobarlo creando un fichero dentro del docker y mirando fuera que realmente está (o al revés):

```
docker exec pythonserver1 bash -c 'echo "$MIVARIABLE" > /output/variable'
cat output/variable
```

Que pasará si ejecutamos lo mismo en el contenedor pythonserver2?

Podríamos también separar los dos servicios en dos ficheros, *pythonserver1.yml* y *pythonserver2.yml*. Entonces podríamos arrancar uno u otro, o los dos como hacemos ahora:

```
docker compose -f pythonserver1.yml up -d
docker compose -f pythonserver1.yml -f pythonserver1.yml up -d
docker compose -f pythonserver1.yml -f pythonserver1.yml config > ps1_ps2.yml
```

Otra opción interesante que tenemos es la de tener un fichero más genérico, y que parte de sus parámetros provengan de un fichero de variables externo. Esto se hace creando un fichero *.env* que contenga las variables a sustituir:

```
SERVIDOR=server1
```

Y entonces en el yml:

```
    environment:
      MIVARIABLE: ${SERVIDOR}
```

Podremos comprobar que se sustituirá generando el fichero resultante de la sustitución con *config*:

```
docker compose config > sustituido.yml
```

Pasemos ahora a evaluar un *docker-compose.yml* de un CMS como el *Wordpress*: [https://github.com/docker/awesome-compose/tree/master/official-documentation-samples/wordpress/](https://github.com/docker/awesome-compose/tree/master/official-documentation-samples/wordpress/)

Vemos que hay cosas similares, pero también algunas nuevas:

- **command**: será el comando a ejecutar inicialmente al arrancar el contenedor.
- **volumes**: internos de docker. Se mapean en /var/lib/docker. Podremos verlos con ```docker volume ls```
- **restart**: política a aplicar cuando el contenedor se pare.
- **expose**: puertos internos entre dockers

Ahora podemos seguir las instrucciones del repositorio, inicial nuestros contenedores para tener un *Wordpress* al que podremos acceder en el puerto 80.

# Ejemplos IsardVDI

## Listado de dominios que no se han usado desde X tiempo

En los dominios tenemos un campo accessed que se actualiza cuando se accede a ese escritorio. Podemos usar esa clave junto la del tipo desktop para filtrar los que no se han usado en un tiempo concreto.


```
import json
import requests
from pprint import pprint
from auth import get_auth_header
import time
# listado de dominios no usados desde hace X días


X = 300

x_segundos = int(time.time() - X*86400)
print(x_segundos)

base = "DOMAIN"ASDF

response = requests.post(
    base + "/admin/table/domains",
    json={"pluck":["id","accessed"],"order_by":"accessed"},
    headers=get_auth_header(),
    verify=True,
)
if response.status_code == 200:
    domains=json.loads(response.text)
else:
    print(" Error contactando con api")
    exit(1)

print(f"Totales: {len(domains)}")
print(domains[0]["accessed"])
dom_dias = [dominio for dominio in domains if dominio["accessed"] >= x_segundos]
print(f"De los últimos {X} días: {len(dom_dias)}")
```


## Acciones sobre dominios

Las acciones habituales sobre un escritorio son:

- Starting: [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L51](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L51)
- Stopping: [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L119](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L119)
- Shutting-down: [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L133](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L133) con el parámetro *force* a *True*.
- Deleting [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L384](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/DesktopsPersistentView.py#L384)

El *engine* al ver estos cambios en la BBDD realizará las acciones correspondientes en los hypervisores.

```
import json
import requests
from pprint import pprint
from auth import get_auth_header
import time

# Start
# /desktop/start/<desktop_id>"


base = "https://[DOMAIN]/api/v3"

response = requests.get(
    base + "/admin/table/domains",
    json={},
    headers=get_auth_header(),
    verify=True,
)
if response.status_code == 200:
    domains=json.loads(response.text)
else:
    print(" Error contactando con api")
    print(response.status_code)
    exit(1)

response = requests.get(
    base + f'/desktop/start/{domains[0]["id"]}',
    json={},
    headers=get_auth_header(),
    verify=True,
)
if response.status_code == 200:
    respuesta=json.loads(response.text)
else:
    print(" Error contactando con api")
    exit(1)

print(respuesta)
```

## Descargar CSV de visores directos de un despliegue

Tenemos un endpoint que nos devolverá el csv [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/deployments/DeploymentsView.py#L111](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/deployments/DeploymentsView.py#L111)


```
import json
import requests
from pprint import pprint
from auth import get_auth_header
import time

# CSV de Deployments

deployment_id = "Deployment"

base = "https://[DOMAIN]/api/v3"

response = requests.get(
    base + f"/deployments/directviewer_csv/{deployment_id}",
    json={},
    headers=get_auth_header(),
    verify=True,
)
if response.status_code == 200:
    csv_data=json.loads(response.text)
else:
    print(" Error contactando con api")
    print(response.status_code)
    exit(1)

print(response.headers)
print(csv_data)


with open("datos.csv","w") as fichero:
    fichero.write(csv_data)
```


## Listado de plantillas con información de sus dominios

Las plantillas tienen siempre unas derivadas, que pueden ser escritorio o otras plantillas derivadas. Eso da un árbol descendiente a partir de esa plantilla. Podremos acceder a esos datos mediante el endpoint
`/api/v3/admin/desktops/tree_list/${template_id}` en [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/AdminDomainsView.py#L117](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/AdminDomainsView.py#L117).


import json
import requests
from pprint import pprint
from auth import get_auth_header
import time
import random

# Template tree

```
"/api/v3/admin/desktops/tree_list/<id>"


base = "https://[DOMAIN]/api/v3"


response = requests.get(
    base + "/admin/table/domains",
    json={},
    headers=get_auth_header(),
    verify=True,
)
if response.status_code == 200:
    domains=json.loads(response.text)
else:
    print(" Error contactando con api")
    print(response.status_code)
    exit(1)

template_id = random.choice([t["id"] for t in domains if t["kind"] == "template"])

response = requests.get(
    base + f"/admin/desktops/tree_list/{template_id}",
    json={},
    headers=get_auth_header(),
    verify=True,
)
if response.status_code == 200:
    temp_tree=json.loads(response.text)
else:
    print(" Error contactando con api")
    print(response.status_code)
    exit(1)


pprint(temp_tree)
```

## Listado de dominios por tamaño de disco

Antíguamente los discos de los domínios se definian dentro del mismo *json* del dominio, en la clave `{"create_dict}`


```
 {
    "create_dict": {
            "disks": [
                {
                    "size": 34435,
                    "file": "/isard/groups/default/default/admin/admin/test.qcow2"
                    "ext": "qcow2",
                    "path": "/isard/groups
                }
            ] , 
...
```

Y ahora se encuentran en su propia tabla de *storage* y en el dominio tenemos su *storage_id*:

```
 {
    "create_dict": {
            "disks": [
                {
                    "storage_id": "3b91e322-6257-480a-bc1f-78aa75c538fd"
                }
            ] , 
...

## Creación de dominios

## Modificación de dominios

## Alta de usuarios en un grupo concreto

## Listado de dominios iniciados sin visor conectado

Estamos haciendo pruebas contra el *prometheus* en [https://gitlab.com/isard/isardvdi/-/merge_requests/2027/](https://gitlab.com/isard/isardvdi/-/merge_requests/2027/)


```
import os
import time
import urllib.parse
from datetime import datetime, timedelta

import requests
from cachetools import TTLCache, cached
from jose import jwt
from rethinkdb import RethinkDB

from api import app

from .._common.api_exceptions import Error
from .flask_rethink import RDB

db = RDB(app)
db.init_app(app)
r = RethinkDB()


base = "https://" + os.environ.get("DOMAIN") + "/prometheus/api/v1/query?query="


@cached(TTLCache(maxsize=1, ttl=5 * 60))
def auth_token_header():
    with app.app_context():
        admin_secret_data = r.db("isard").table("secrets").get("isardvdi").run(db.conn)
    raw_jwt_data = {
        "exp": datetime.utcnow() + timedelta(minutes=5),
        "kid": admin_secret_data["id"],
        "iss": "isard-authentication",
        "data": {
            "role_id": admin_secret_data["role_id"],
            "category_id": admin_secret_data["category_id"],
            "user_id": "local-default-admin-admin",
            "group_id": "default-default",
        },
    }
    admin_jwt = jwt.encode(raw_jwt_data, admin_secret_data["secret"], algorithm="HS256")
    return {"Authorization": "Bearer " + admin_jwt}


def desktops_started():
    # Should we check if any is started?
    query = f'(count(present_over_time(isardvdi_domain{{status="Started"}}[1m])) or vector(0))'


# Prometheus queries
################## Viewers ##################
def prom_viewer(desktop_id, seconds, viewer_type=None):
    query_spice = f'(count(present_over_time(isardvdi_domain_port_spice_tls{{desktop="{desktop_id}"}}[{seconds}s]) * on(desktop) group_left isardvdi_domain_port_spice_tls{{desktop="{desktop_id}"}} * on (port,hypervisor) group_left count_over_time(isardvdi_socket_recv_bytes{{}}[{seconds}s])) or vector(0))'
    query_vnc = f'(count(present_over_time(isardvdi_domain_port_vnc{{desktop="{desktop_id}"}}[{seconds}s]) * on(desktop) group_left isardvdi_domain_port_vnc{{desktop="{desktop_id}"}} * on (port,hypervisor) group_left count_over_time(isardvdi_socket_recv_bytes{{}}[{seconds}s])) or vector(0))'
    query_rdp = f'(count(present_over_time(isardvdi_domain_net_rx_bytes{{desktop="{desktop_id}"}}[{seconds}s]) * on (mac) group_left count_over_time(isardvdi_conntrack_rdp_recv_bytes{{}}[{seconds}s])) or vector(0))'

    if not viewer_type:
        query = query_spice + "+" + query_vnc + "+" + query_rdp
    elif viewer_type == "spice":
        query = query_spice
    elif viewer_type == "vnc":
        query = query_vnc
    elif viewer_type == "rdp":
        query = query_rdp
    else:
        raise Error("bad_request", "Invalid viewer type")

    query = urllib.parse.quote(query)
    response = requests.get(
        base + query,
        json={},
        headers=auth_token_header(),
        verify=True,
    )
    if response.status_code == 200:
        return (
            True
            if response.json().get("data", {}).get("result")[0]["value"][1] != "0"
            else False
        )
    else:
        return None


def prom_viewers(seconds=40):
    query_spices = f"present_over_time(isardvdi_domain_port_spice_tls{{}}[{seconds}s]) * on(desktop) group_left isardvdi_domain_port_spice_tls{{}} * on (port,hypervisor) group_left count_over_time(isardvdi_socket_recv_bytes{{}}[{seconds}s])"
    query_vncs = f"present_over_time(isardvdi_domain_port_vnc{{}}[{seconds}s]) * on(desktop) group_left isardvdi_domain_port_vnc{{}} * on (port,hypervisor) group_left count_over_time(isardvdi_socket_recv_bytes{{}}[{seconds}s])"
    query_rdps = f"present_over_time(isardvdi_domain_net_rx_bytes[{seconds}s]) * on (mac) group_left count_over_time(isardvdi_conntrack_rdp_recv_bytes[{seconds}s])"

    queries = {"spice": query_spices, "vnc": query_vncs, "rdp": query_rdps}
    results = {}
    stamp = int(time.time())
    for protocol, query in queries.items():
        response = requests.get(
            base + urllib.parse.quote(query),
            json={},
            headers=auth_token_header(),
            verify=True,
        )
        if response.status_code == 200:
            result = response.json()["data"]["result"]
            if len(result):
                for r in result:
                    if r["metric"]["desktop"] in results:
                        results[r["metric"]["desktop"]]["viewer"]["protocols"].append(
                            protocol
                        )
                        results[r["metric"]["desktop"]]["since"] = stamp
                    else:
                        results[r["metric"]["desktop"]] = {
                            "id": r["metric"]["desktop"],
                            "category_id": r["metric"]["category_id"],
                            "group_id": r["metric"]["group_id"],
                            "user_id": r["metric"]["user_id"],
                            "viewer": {"since": stamp, "protocols": [protocol]},
                        }
        else:
            return None
    return [results[k] for k in results]
```
# Contenedor Python

Podemos usar la imágen oficial de python para ejecutar nuestro contenedor. Pero primero deberemos tener una aplicación Python. 

## Aplicación web

En este ejemplo vamos a crear una aplicación Flask, un servidor web simple en un fichero `app.py`

```Python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_docker():
    return 'Hello, Docker!'
```

Tal como vemos estamos usando la librería *flask* que deberemos instalar:

```
pip3 install Flask
```

Y seguidamente podremos iniciar nuestro servidor web con esta simple aplicación:

```
python3 -m flask run --host=0.0.0.0 --port=80
```

Si nos conectamos por el navegador web a la ip dónde lo estamos ejecutando deberíamos ver `Hello, Docker!`

## Probando en docker

Es útil probar antes de realizar la creación del fichero `Dockerfile` que nos servirá para crear nuestra imágen docker con nuestra aplicación y librerías. Para ello podemos ejecutar un contenedor basado en la imágen de docker que vamos a usar.

Esto también nos servirá para ver qué utilidades vienen ya dentro de la imágen.

```
docker run -ti python:3.9 /bin/bash
```

Ahora estamos dentro del contenedor creado a partir de la imagen `python:3.9`. Intentamos editar un fichero nuevo con `nano` y con `vi` y vemos que ninguno de los dos editores se encuentra dentro. Recordemos que las imágenes intentan ser lo más simples posibles.

Miramos en que distribución está basado para saber como instalar paquetes:

```
root@f4f30799ef9a:/# cat /etc/os-release 
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
```

Por lo tanto instalamos el editor que queramos con `apt install nano`. Veremos que nos da error. Esto es también debido a que al crear la imágen borraron los índices descargados de actualizaciones de paquetes, que deberemos volver a descargar con `apt update`.

Creamos un nuevo fichero `app.py` y copiamos dentro el código anterior de nuestra aplicación flask y procedemos a ejecutarlo. Veremos que nos da error, porque el paquete `flask` no está instalado. Lo instalaremos dentro y comprobaremos de nuevo que podemos arrancar nuestra aplicación.

Si todo ha sido correcto la aplicación debería iniciar:

```bash
root@f4f30799ef9a:/# python3 -m flask run --host=0.0.0.0 --port=80
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on all addresses (0.0.0.0)
 * Running on http://127.0.0.1:80
 * Running on http://172.17.0.2:80
Press CTRL+C to quit
```

Antes de salir del contenedor nos interesa saber qué versión del paquete flask se ha instalado, ya que para crear nuestra nueva imágen también deberemos definirlo.

```
root@f4f30799ef9a:/# python3 -m pip freeze
blinker==1.6.2
click==8.1.3
Flask==2.3.2
importlib-metadata==6.6.0
itsdangerous==2.1.2
Jinja2==3.1.2
MarkupSafe==2.1.2
Werkzeug==2.3.4
zipp==3.15.0
```

Copiaremos el listado en un fichero en local de nombre `requirements.pip` y procederemos a salir del contenedor y crear nuestra imágen.

La estructura que deberemos tener es:

```
.
├── app
│   └── app.py
├── Dockerfile
└── requirements.pip
```

# Imágen docker

Para crear nuestra imágen deberemos primero crear el fichero `Dockerfile` que la define:

```
FROM python:3.9
WORKDIR /app
COPY requirements.pip requirements.pip
RUN pip3 install -r requirements.pip
COPY app .
CMD ["python3", "-m" , "flask", "run", "--host=0.0.0.0"]
```

Aquí nos ponemos en el nuevo directorio `app`, instalamos los paquetes del fichero `requirements.pip` y copiamos nuestro script de la carpeta local `app` también dentro y finalmente iniciamos el servidor.

# Docker compose

Una vez hecho esto podremos pasar a crear nuestra imágen, pero lo vamos a hacer con `docker compose`. Para ello crearemos nuestro fichero `docker-compose.yml`:

```
version: '3.5'
services:
  app:
    container_name: app
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - published: 80
        target: 80
```
Vemos que hemos abierto el puerto 80, ya que sinó no podremos acceder externamente al docker a ese puerto.

Crearemos la imágen con `docker compose build` y lo iniciaremos con `docker compose up -d`

En este punto deberíamos tener un contenedor `app` ejecutándose con nuestra aplicación y deberíamos poder acceder al web igual que antes.


Para desarrollo de la aplicación es habitual montar el volumen con la aplicación dentro, así los cambios que hagamos fuera se reflejan dentro.

```
version: '3.5'
services:
  app:
    container_name: app
    build:
      context: .
      dockerfile: Dockerfile
    ports:
      - published: 80
        target: 80
    volumes:
      - app:/app:rw
```
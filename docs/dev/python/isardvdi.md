# Python en IsardVDI

En diferentes contenedores tenemos código python. Estos contenedores són:

- **isard-api**: Proporciona una estructura de vistas (views) y de librería (lib). En las vistas estarán los *endpoints* con el control de acceso. Estos *endpoints* llaman a funciones o clases de la libreria para realizar las acciones.
- **isard-hypervisor**: Tiene algunas funciones python para registrarse contra isard-api y también para analizar la conectividad de la VPN de hypervisores y gestionar los certificados que recibe de la api.
- **isard-storage**: Es un nuevo contenedor cuyo código python está destinado a realizar las funciones de discos virtuales, con librerias python como la `guestfs` o la `pyqcow`.
- **isard-vpn**: Tiene una serie de scripts que permiten gestionar el concentrador Open vSwitch y las reglas de filtrado iptables en tiempo real.
- **isard-scheduler**: Contenedor que tiene implementada la libreria `apscheduler` de python que permite programar acciones.

## Python en isard-hypervisor

Vamos a analizar este script de *isard-hypervisor*:

[https://gitlab.com/isard/isardvdi/-/blob/main/docker/hypervisor/src/lib/vpnc.py](https://gitlab.com/isard/isardvdi/-/blob/main/docker/hypervisor/src/lib/vpnc.py)

Este script usa los paquetes `time` y `pythonping` que hemos visto antes para comprobar que se ha establecido una conexión VPN `wireguard` entre el *isard-hypervisor* y el concentrador *isard-vpn*.

También podemos ver que hace uso de la función `check_output` del paquete `subprocess`. Este nos permitirá ejecutar órdenes del sistema operativo y recojer su resultado si quisiéramos analizarlo, como hacemos por ejemplo en [este código](https://gitlab.com/isard/isardvdi/-/blob/main/docker/storage/api/api/libv2/storage/isard_qcow.py#L64) de *isard-storage*.

Es [este script](https://gitlab.com/isard/isardvdi/-/blob/main/docker/hypervisor/src/lib/del_wireguard.py) de *isard-hypervisor* podemos ver como también podemos usar la libreria estándard `os` para lanzar órdenes de sistema operativo o también para escribir ficheros en disco.

## Peticiones con autenticación API

Hay diversos contenedores python como `isard-hypervisor` que se conectan a isard-api mediante API REST para intercanviar datos. Incluso `isard-api` también se conecta a otros contenedores:

- isard-hypervisor
- isard-vpn
- isard-storage
- isard-scheduler

Todos ellos hacen uso de código para realizar *requests* como las que hemos visto en ejemplos anteriores. La diferencia aquí es que no siempre todos estos contenedores se encuentran en el mismo servidor y pueden realizar las peticiones en la red interna de docker, que por concepto ya es segura.

Para comunicaciones entre servidores es necesaria una capa de encriptación (HTTPS/TLS) y de autorización (cabecera).

Existen [diferentes mecanismos](https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication#authentication_schemes) para añadir en las cabeceras de la petición HTTP los datos de usuario. Entre los más comunes encontramos **basic** y **bearer**.

### Autenticación `basic`

Añade en la cabecera el usuario y password codificados en *base64*

```
Authorization: Basic ZGVtbzpwQDU1dzByZA==
```

### Autenticacion `bearer`

Una vez el usuario está autenticado, normalmente mediante un login, el servidor proporciona un *token* que el cliente debe hacer servir en la cabecera para sus siguientes peticiones.

```
Authorization: Bearer < token >
```

Este token puede basarse en diferentes estándares para ser generado, pero todos se basan en el estándard JWT (JSON Web tokens). Antes del JWT se usaba un *token* aleatorio, pero el JWT definió como se podía añadir información (JSON) a ese *token*.

Por ejemplo podemos ir al `local storage` de la consola del navegador una vez logeados en IsardVDI y encontraremos una entrada con el *token*:

    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODQ5NzA5NzEsImlzcyI6ImlzYXJkLWF1dGhlbnRpY2F0aW9uIiwia2lkIjoiaXNhcmR2ZGkiLCJkYXRhIjp7InByb3ZpZGVyIjoibG9jYWwiLCJ1c2VyX2lkIjoibG9jYWwtZGVmYXVsdC1hZG1pbi1hZG1pbiIsInJvbGVfaWQiOiJhZG1pbiIsImNhdGVnb3J5X2lkIjoiZGVmYXVsdCIsImdyb3VwX2lkIjoiZGVmYXVsdC1kZWZhdWx0IiwibmFtZSI6IkFkbWluaXN0cmF0b3IifX0.SguYiFj1I-LYUz_KZrkTspJd5pbwcTs4LmeeWq0ddxY

En el web [https://jwt.io/](https://jwt.io/) podemos pegar ese token y veremos qué datos contiene en formato JSON:

- *CABECERA* (HEADER): Indica el algoritmo usado para hacer el *hash* y el tipo de *token*. El *hash* se hace con los datos y con un *SECRET*, que en IsardVDI definimos en el [isardvdi.cfg](https://gitlab.com/isard/isardvdi/-/blob/main/isardvdi.cfg.example#L66).

```
    {
    "alg": "HS256",
    "typ": "JWT"
    }
```

- *DATOS* (PAYLOAD): Son datos que ya están establecidos (exp, iss, kid) o claves propias como el campo *data*:

```
    {
    "exp": 1684970971,
    "iss": "isard-authentication",
    "kid": "isardvdi",
    "data": {
        "provider": "local",
        "user_id": "local-default-admin-admin",
        "role_id": "admin",
        "category_id": "default",
        "group_id": "default-default",
        "name": "Administrator"
    }
    }
```

El hecho de que en todas las peticiones el cliente tenga que enviar esa cabecera (que guarda en el *storage* del navegador) hace que en todo momento el cliente esté identificado y tenga datos que no hace falta volver a consultar a la base de datos.

El campo *exp* viene de `expiration` e indica hasta cuando este *token* es válido. Una vez pasada la fecha el token queda invalidado y no será aceptado por el servidor. Podemos comprobar la fecha de caducidad de un token en la web [https://www.epochconverter.com/](https://www.epochconverter.com/):

```
GMT: Wednesday 24 May 2023 23:29:31
Your time zone: jueves 25 de mayo de 2023 a las 1:29:31 GMT+02:00 DST
Relative: In 4 hours
```

Podemos ver que en IsardVDI por defecto tenemos un tiempo de JWT de 4h que es configurable en [isardvdi.cfg](https://gitlab.com/isard/isardvdi/-/blob/main/isardvdi.cfg.example#L89)

### Ejemplo

Vamos a hacer una petición como las que hemos realizado anteriormente a la API REST de `isard-api`:

NOTA: Cambiar la variable *DOMINIO* por un dominio en dónde esté instalado IsardVDI de pruebas.

```Python
import requests

DOMINIO="CHANGE.ME"
response = requests.get(f"https://{DOMINIO}/api/v3/admin/users")
if response.status_code == 200:
    print("Tenemos acceso")
else:
    print("Algo fué mal")
    print("Código de respuesta: {response.status_code}")
```

Nos debería devolver un código **401** de `unauthorized` ya que se trata de un *endpoint* protegido con *JWT*. Para consultar el código de respuesta podemos mirar en los [estándares](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status).


Para poder acceder a ese *endpoint* deberemos añadir un *bearer JWT* válido para esa instalación, que deberemos firmar con el *SECRET* de esa instalación y que encontraremos en [isardvdi.cfg](https://gitlab.com/isard/isardvdi/-/blob/main/isardvdi.cfg.example#L66).

Para ello deberemos instalar la libreria python de *jose* con `pip3 install jose`.

```Python
import requests
from time import sleep
from datetime import datetime, timedelta
from jose import jwt

API_ISARDVDI_SECRET = "kpWpdF0NtI2XCEfzMp36hdSV9S42E7axS8D5TvP9c0A="
JSON_DATA = {
    "exp": datetime.utcnow() + timedelta(seconds=3),
    "kid": "isardvdi",
    "data": {
        "role_id": "admin",
        "category_id": "default",
        "group_id": "default-default",
        "name": "Administrator",
    },
}
admin_jwt = jwt.encode(JSON_DATA, API_ISARDVDI_SECRET, algorithm="HS256")

DOMINIO = "CHANGE.ME"
response = requests.get(
    f"https://{DOMINIO}/api/v3/admin/users",
    headers={"Authorization": "Bearer " + admin_jwt},
)
if response.status_code == 200:
    print("Tenemos acceso")
else:
    print("Algo fué mal")
    print(f"Código de respuesta: {response.status_code}")

sleep(6)
response = requests.get(
    f"https://{DOMINIO}/api/v3/admin/users",
    headers={"Authorization": "Bearer " + admin_jwt},
)
if response.status_code == 200:
    print("Tenemos acceso")
else:
    print("Algo fué mal")
    print(f"Código de respuesta: {response.status_code}")
```

En esta secuencia veremos que el *token* con validez de 3 segundos ha sido aceptado en la primera petición, pero que después de esperar 6 segundos ya no lo ha dado por válido.

Para recoger los datos válidos deberemos usar como anteriormente el método `response.json()` que vimos anteriormente.

Podemos ahora hacer un ejemplo enviando nuevos datos (métodos POST/PUT) a la api.

## Acediendo a RethinkDB

Algunos contenedores también realizan conexiones a la base de datos [RethinkDB](https://rethinkdb.com/) que se usa en IsardVDI.

Se trata de una base de datos documental, en dónde los documentos son entradas JSON en tablas. Esto permite una perfecta integración con python que trabaja muy bien con los diccionarios. Esta base de datos también dispone de los [changefeeds](https://rethinkdb.com/docs/changefeeds/python/) que generan eventos cuando hay cambios en entradas de las tablas de la BBDD. Estos cambios, junto con los *websockets* usados en el *frontend* proporiconan el tiempo real al usuario.

Para acceder a la BBDD nos hará falta la libreria de Python correspondiente y también que tengamos acceso a ella por el puerto por defecto 28015/TCP. Una opción es ejecutar el código dentro de uno de los contenedores python de IsardVDI o bien desde el servidor en dónde se están ejecutando los contenedores, usando la [dirección IP](https://gitlab.com/isard/isardvdi/-/blob/main/docker-compose-parts/db.yml#L14) del contenedor de *isard-db*.

Primero instalaremos el conector para rethink con `pip3 install rethinkdb==2.4.9`.

```Python
from rethinkdb import r
from pprint import pprint

dbconn = r.connect("isard-db", 28015, "isard").repl()

users = r.table("users").run(dbconn)
pprint(users)
```

El resultado que obtendremos debería ser similar al obtenido anteriormente al acceder via API REST.

En RethinkDB podemos acotar los datos que pedimos:

- **get(id)**: para pedir el documento que tiene de clave principal el valor de la variable `id`.
- **filter({"category":category})**: para pedir la lista de todos los documentos que tienen en el campo `category` el valor de la variable `category`
- **get_all(category, index="category")**: nos devolverá lo mismo que con el filtro anterior, pero en este caso hace uso de `índices`, que deben ser creados a priori, y que permiten filtrados más rápidos de los documentos.

También podremos actualizar datos con `update({"category":nueva_categoria})`, que reemplazará en el documento, o en la lista de documentos, por `nueva_categoria`.

Para insertar nuevos documentos lo haremos con `insert(diccionario)`, en dónde el diccionario será convertido a un documento JSON como nueva entrada en la tabla en RethinkDB. Si el diccionario tiene la clave principal de documento, se usará esa. Si no la lleva entonces la BBDD la añadirá con un formato de `uuid64`.

Para eliminar documentos, seleccionaremos uno o más y añadiremos `delete()`.

También dispone de la posibilidad de mezclar nuevos campos al resultado con `merge({"dato":"test"})` o bien coger esos datos de otra tabla mediante `lambda`  y `r.row`.

En cuanto a recoger eventos de tipo `changefeed`, deberemos hacer una consulta y añadir al final `changes()`. Esto hará que el código se quede esperando ahí y saldrá cuando aparezca un nuevo dato que concuerde con la consulta. Es por esto que es necesario que este código esté dentro de un bucle y, para no bloquear al resto del código, también deberá ejecutarse en un hilo (thread) a parte.


## Estructura API

El código del contenedor de API es el que hace de interfaz del sistema con el mundo exterior tal como hemos visto. Su estructura es:

```
├── api
│   ├── _common
│   ├── __init__.py
│   ├── libv2
│   ├── __pycache__
│   ├── schemas
│   ├── static
│   └── views
```

En dónde las dos carpetas principales son:

- **views**: Rutas Flask con decoradores de autenticación JWT.
- **libv2**: Funciones y clases con las acciones que piden las vistas.

Y también tenemos la carpeta **schemas** que es en dónde tenemos las definiciones de datos que se pueden enviar a las *vistas* en `yml` de [Cerberus](https://docs.python-cerberus.org/en/stable/index.html).

Algunas funciones son compartidas por diferentes contenedores, como las de control de errores y acceso via api entre contenedores. Estas se encuentran fuera del código de `api` (en la carpeta *component*) y son montadas como volúmenes en cada contenedor para que puedan ser importadas (_common).


## Estructura Webapp

Webapp és una aplicación python Flask que actualmente renderiza la parte de administración (js/html). Los datos que se muestran y las acciones que se realizan van contra la *API*. Su estructura es:

```
└── webapp
    ├── auth
    ├── _common
    ├── config
    ├── __init__.py
    ├── lib
    ├── node_modules
    ├── package.json
    ├── __pycache__
    ├── static
    ├── templates
    ├── views
    └── yarn.lock
```

En dónde las carpetas principales que nos interesan son:

- **views**: La renderización de las vistas
- **templates**: Los html de las vistas
- **static**: Los js de IsardVDI de los que hacemos includes en los html.

Las librerias *js** externas se definen el el fichero *packages.json* y son gestionadas con *node* en la creación del contenedor:

```
{
  "dependencies": {
    "font-linux": "^0.6.1",
    "gentelella": "^1.4.0",
    "socket.io": "^4.1.3"
  }
}
```

Entre las librerias más usadas en webapp tenemos [Datatables](https://datatables.net) usada para mostrar datos en tablas a partir de las respuestas de peticiones a *API*.

## Ejemplo práctico

Nos vamos a crear nuestra propia función para realizar peticiones a *API* que pondremos en un fichero `auth.py`:

```
from datetime import datetime, timedelta
from jose import jwt

API_ISARDVDI_SECRET = "kpWpdF0NtI2XCEfzMp36hdSV9S42E7axS8D5TvP9c0A="

def get_auth_header():
    JSON_DATA = {
        "exp": datetime.utcnow() + timedelta(seconds=3),
        "kid": "isardvdi",
        "data": {
            "role_id": "admin",
            "category_id": "default",
            "group_id": "default-default",
            "name": "Administrator",
        },
    }
    admin_jwt = jwt.encode(JSON_DATA, API_ISARDVDI_SECRET, algorithm="HS256")
    return {"Authorization": "Bearer " + admin_jwt}
```

### Listado de dominios de un grupo via API

```
import json
import requests
from .auth import get_auth_header

print("listado de dominios de un grupo")

response = requests.get(
    base + "/admin/table/groups",
    json={},
    headers=get_auth_header(),
    verify=False,
)
if response.status_code == 200:
    groups=json.loads(response.text)
else:
    print(" Error contactando con api")
    exit(1)

print(f"Hemos encontrado {len(groups)} grupos")

for group in groups:
    print("Grupo: " + group["name"])
    response = requests.get(
        base + "/admin/table/domains",
        json={},
        headers=get_auth_header(),
        verify=False,
    )
    if response.status_code == 200:
        print("  -> tiene {len(json.loads(response.text))} dominios")
    else:
        print(" Error contactando con api")
        exit(1)
```

### Listado de dominios de un grupo via BBDD

Podemos extraer los mismos datos accediendo directamente a la BBDD:

```
import json
from pprint import pprint


from rethinkdb import r
dbconn = r.connect("isard-db", 28015, "isard").repl()

print("listado de dominios de un grupo")

groups = r.table("groups").run(dbconn)
groups = list(groups)

print(f"Hemos encontrado {len(groups)} grupos")

for group in groups:
    group_id = group["id"]
    dominios = list(r.table("domains").get_all(group_id, index="group").run(dbconn))
    print("  -> tiene {len(dominios)} dominios")
```

### Filtrando datos

Si queremos solo los escritorios o las plantillas deberemos filtrar el resultado de la tabla *domains* por esa clave `{"kind": "desktop"}` o `{"kind": "template"}`. Podremos hacerlo de diferentes formas.

#### Filtrando en python

Una vez tenemos los resultados podemos filtrar en python:

```
desktops = [domain for domain in domains if domain["kind"] == "desktop"]
```

#### Filtrando en API

Si el *endpoint* al que estamos atacando permite enviar datos de filtrado podremos añadirlos en la petición en el campo *json*. 

Si miramos en el código veremos lo que espera recibir ese *endpoint* [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/AdminTablesView.py#L39](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/AdminTablesView.py#L39).

Vemos que no tiene la opción de añadir un filtro de datos en el mismo *endpoint*, con lo cual deberemos optar por procesar en python a posteriori los datos o buscar otro *endpoint* más específico para filtrdo de dominios [https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/AdminDomainsView.py#L33](https://gitlab.com/isard/isardvdi/-/blob/main/api/src/api/views/AdminDomainsView.py#L33).

```
    response = requests.post(
        base + "/admin/domains",
        json={"kind": "desktop"},
        headers=get_auth_header(),
        verify=False,
    )
```

Por ejemplo, si en lugar de *get* usamos *post* en el *endpoint* genérico inicial `/api/v3/admin/table/domains` podremos pasarle un diccionario con, por ejemplo, campos de los que queremos hacer `pluck`, para que sólo devuelva esos por cada entrada:

```
    response = requests.post(
        base + "/admin/table/domains",
        json={"pluck": ["id", "name", "accessed", "status"]},
        headers=get_auth_header(),
        verify=False,
    )
```

#### Filtrando en BBDD

Al hacer la consulta a la BBDD podremos filtrar por campos:

```
desktops = list(r.table("domains").get_all(group_id, index="group").filter({"kind": "desktop"}).run(dbconn))
```

O usar índices compuestos si los tenemos. En este caso necesitaríamos un índice compuesto por las claves "group" y "kind" que no existe. La definición de las tablas y sus índices se realiza en el *populate.py* o posteriormente en el *upgrade.py* en engine []()

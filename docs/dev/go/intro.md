# Golang

Golang es un lenguaje de programación, tipado y compilado. Esto le da mejor rendimiento que un lenguaje interpretado (como podría ser Python) pero no es tan flexible (que penaliza la velocidad de desarrollo, pero le da estabilidad y robusteza). También tiene una gran librería estándar

## Instalación y configuración del editor

Para pruebas rápidas, existe el [Playground de Go](https://play.golang.com)

Para Go:
```sh
wget https://go.dev/dl/go1.20.5.linux-amd64.tar.gz
rm -rf /usr/local/go
sudo tar -C /usr/local -xzf go1.20.5.linux-amd64.tar.gz
vim ~/.bashrc
...
export PATH=$PATH:/usr/local/go/bin
...
go version
```

VSCodium + extensión:
```sh
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
sudo apt update && sudo apt install codium
codium --install-extension golang.go
```

Finalmente, abrimos el editor, abrimos un fichero `.go` y le damos a `Install All`

## Sintaxis rápida

Para más ejemplos, [https://gobyexample.com](https://gobyexample.com) y [https://go.dev/tour](https://go.dev/tour)

```go
// Esto es un comentario
var hola string = "IsardVDI" // Creamos una variable de tipo 'string' llamada 'hola' con el valor 'IsardVDI'
const formacion = "Go"       // Creamos una constante, el tipo lo reconoce automáticamente el compilador

// Definimos la función 'hola2', que no coge ningún parámetro y no devuelve nada
func hola2() {
	fmt.Println("Hola!") // fmt.Println es una función del paquete 'fmt', que sirve para imprimir por pantalla
}

// Definimos el tipo "mensaje", que guarda varios valores. Es una estructura, que sería similar a una "clase"
type Mensaje struct {
	ID    string
	Autor string
	Hora  time.Time // time.Time es una representación de un momento concreto en el tiempo, es parte del paquete 'time'
	Texto string
}

// Las estructuras pueden tener funciones relacionadas. En este caso la estructura 'Mensaje' tiene la función 'String', que nos devuelve el mensaje 'bonito'
func (m *Mensaje) String() string {
	// La función 'fmt.Sprintf' sirve para formatear strings. '%s' se refiere a que queremos formatear un string,
	// y 'm.Texto' y 'm.Hora' se refieren a los valores del mensaje (equivalente a 'self' del Python). Hemos definido el 'nombre' 'm' arriba.
	// 'mBonito := <...>' es equivalente a 'var mBonito = <...>'
	mBonito := fmt.Sprintf("%s, %s", m.Texto, m.Hora)

	return mBonito
}

// Definimos una función que recibe un 'slice' (similar a una lista) de mensajes
func imprimirAlertasMensajes(msg []*Mensaje) {
	// Iteramos por cada mensaje, y le asignamos el nombre 'm'
	// Si asignamos '_' como nombre a cualquier cosa, le decimos al
	// compilador que ignore el valor. En este caso, ignoramos la posición
	// del mensaje en la lista
	for _, m := range msg {
		// Comprobamos que el texto del mensaje tenga como prefijo 'Alerta: '
		if strings.HasPrefix(m.Texto, "Alerta: ") {
			// Llamamos a la función que hemos definido antes e
			// imprimimos el resultado por pantalla
			fmt.Println(m.String())
		}
	}
}
```

## Estructura y paquetes

Go organiza los programas por paquetes. Cada paquete es una carpeta. Se define el nombre del paquete a la cabecera del fichero:

```go
package mensaje
```

Si os habéis fijado en el ejercicio anterior, hay declaraciones que empiezan con mayúscula y otras con minúscula. Esto indica si la definición es pública (mayúscula) o privada (minúscula). Si es pública, se puede acceder desde otro paquete, si es privada no.

```go
const privado = "sólo accesible desde el mismo paquete"
const Público = "accesible desde todos los paquetes" // Go es utf-8, se pueden poner acentos y cualquier carácter en las variables!

type Hola struct {
    Accesible string // Si le pasamos 'Hola' a otro paquete, podrá ver 'Accesible'
    noAccessible string // El resto de paquetes no verán 'noAccessible'
}
```

Todas las definiciones privadas se pueden acceder desde todo el paquete (distintos ficheros dentro de la misma carpeta).

```
proyecto/
    mensaje/
        mensaje.go // -> Definimos el tipo 'mensaje'
        procesado.go // -> Podemos acceder al tipo 'mensaje'
```


## Mini ejercicio: servidor HTTP simple

Vamos a crear un servidor HTTP muy simple:

```sh
cd /tmp
mkdir test-http
cd $_
go mod init test-http
```

```go
package main // El paquete que se ejecutará, se tiene que llamar 'main'

// Importamos los paquetes que utilizaremos de la librería estándar
import (
	"log"
	"net/http"
)

// Definimos una función que escribe una respuesta http
func hola(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hola! :)")) // La respuesta es de tipo []byte (lista de bytes)
}

func main() {
    // Si nos piden '/hola', que lo gestione la función 'hola' que hemos definido arriba
	http.HandleFunc("/hola", hola)

    // Todo el resto, que lo gestione una función 'anónima' (sin nombre), que pasamos como argumento a la función 'http.HandleFunc'
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        // La respuesta será un código HTTP 400
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("No saludas? 🤨🤨"))
	})

    log.Println("escuchando en el puerto :8080")
    // Escuchamos en el puerto :8080, pero si hay un error...
	if err := http.ListenAndServe(":8080", nil); err != nil {
        // Lo mostramos y cerramos el programa
		log.Fatalf("error al crear el servidor http: %v", err)
	}
}
```

Y lo ejecutamos:

```sh
go run main.go
```

## Ejemplo llamada API Isard

Queremos escribir un programa que llame a la API para consultar la versión de Isard que tiene instalada.

Tenemos el endpoint `/api/v3` que no responde con una estructura tal que así:
```json
{
    "name": "IsardVDI",
    "api_version": 3.1,
    "isardvdi_version": "tags/v10.71.2-dirty",
    "usage": "production"
}
```

```go
package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
)

// Esta estructura es la estructura de datos de la respuesta de la API
type isardVersion struct {
    // Aquí utilizamos las anotaciones de Go. Son los valores que van entre acentos (``). Sirven
    // para indicar cosas a los paquetes de Go y que lo puedan leer. En el caso de JSON, sirve para
    // indicar el nombre del campo que tendrá el JSON para que lo lea automáticamente. Por ejemplo,
    // el campo de JSON de la respuesta 'isardvdi_version' guardará sus valores al campo 'Version'
    // de nuestra estructura. Fijaos que los campos són en majúsculas (públicos), si no, el paquete
    // 'json' no puede acceder a ellos
	Name       string  `json:"name"`
	APIVersion float32 `json:"api_version"`
	Version    string  `json:"isardvdi_version"`
	Usage      string  `json:"usage"`
}

func main() {
    // Queremos coger el primer argumento que le pasemos al programa como 
    // la instancia de Isard
	if len(os.Args) == 1 {
		log.Fatal("Please, pass the IsardVDI as argument (https://isard.instance.example.com)")
	}
	host := os.Args[1]

    // Parseamos la URL que nos han pasado para comprobar que es correcta y
    // poder trabajar con la estructura url.URL (del paquete 'url')
	u, err := url.Parse(host)
	if err != nil {
		log.Fatalf("invalid IsardVDI url (must be https://isard.example.com): %v", err)
	}
    // Especificamos el endpoint correcto
	u = u.JoinPath("/api/v3")

    // Hacemos una petición HTTP GET a la URL que hemos creado antes
	rsp, err := http.Get(u.String())
	if err != nil {
		log.Fatalf("query IsardVDI: %v", err)
	}
    // Defer es una palabra especial, que especifica que la función a continuación
    // se ejecutará al final de la función actual. En este caso, cuando se termine
    // el programa, queremos asegurarnos que no nos queda memoria abierta de
    // la respuesta. El garbage collector de Go lo limpiaría igualmente, pero
    // como no siempre es el caso en todos los programas, es mejor acostumbrarse
    // a ir cerrando los recursos cuando ya no se usan
	defer rsp.Body.Close()

    // Creamos una variable sobre la cual decodificaremos el JSON que hemos recibido
    // como respuesta
	v := isardVersion{}

    // Creamos un decodificador que lee del cuerpo de la respuesta HTTP y lo envía
	// a la variable que justo hemos creado
	if err := json.NewDecoder(rsp.Body).Decode(&v); err != nil {
		log.Fatalf("decode JSON response: %v", err)
	}

    // Mostramos los valores por pantalla
	fmt.Printf("Name: %s\n", v.Name)
	fmt.Printf("API Version: %g\n", v.APIVersion)
	fmt.Printf("IsardVDI Version: %s\n", v.Version)
	fmt.Printf("Usage: %s\n", v.Usage)
}
```

## Punteros y referencias

Go es un lenguaje que la memoria se gestiona automáticamente, por su runtime (quien te da la memoria) y garbage collector (quien la libera cuando ya no es necesaria). Aunque parezca magia y no nos hayamos de preocupar por ello, por abajo la representación es la misma que en C, con una tabla de memoria.

| Dirección | Valor |
| --------- | ----- |
| 0x000000  | 3     |
| 0x000001  | 7     |
| ...       | ...   |

Para qué nos sirve saber esto? Para diferenciar entre dirección de memoria y su valor. Tomemos el siguiente ejemplo:

```go
package main

type Message struct {
	Content string
}

func main() {
	// Aquí Go nos pide y guarda en memoria "hola" automáticamente
	content := "hola"

	// Para hacernos una idea, sería algo como esto (es un ejemplo para entenderlo,
	// no es una representación real)
	/* 

	| Dirección | Valor |
	| --------- | ----- |
	| 0x000000  | h     |
	| 0x000001  | o     |
	| 0x000002  | l     |
	| 0x000003  | a     |
	| ...       | ...   |

	*/

	// Ahora, vamos a inicializar una estructura
	msg := Message{Content: content}

	// Cómo queda en memoria esto?
	// Ha copiado el *valor* de la variable `content`?
	// O lo ha referenciado?

	fmt.Println(content)
	fmt.Println(msg.Content)
	// Ojo que las direcciones son distintas!
	fmt.Println(&content)
	fmt.Println(&msg.Content)

	content = "qué tal? :)"

	fmt.Println(content)
	fmt.Println(msg.Content)
	fmt.Println(&content)
	fmt.Println(&msg.Content)

	// Y el contenido también

	// Miremos la memoria!
	/* 

	| Dirección | Valor   |
	| --------- | -----   |
	| 0x000000  | h       |
	| 0x000001  | o       |
	| 0x000002  | l       |
	| 0x000003  | a       |
	| 0x000004  | Message |
	| 0x000005  | Content |
	| 0x000006  | h       |
	| 0x000007  | o       |
	| 0x000008  | l       |
	| 0x000009  | a       |
	| ...       | ...     |

	*/

	// La ha copiado!! Por qué?
	// Siempre que pasemos variables arriba y abajo, estaremos pasando el *valor* que contienen
	// Normalmente, esto nos interesa, pero a veces no. Si queremos que no se copie, tenemos que
	// utilizar *punteros*. Los punteros son, simplemente, una flecha en la tabla de memoria.
	// Lo que hacen es decir: "el valor está en esta dirección"
	// Vamos a crear otro programa...
}
```

```go
package main

type Message struct {
	// Con el carácter `*` especificamos que `Content` es de tipo "puntero que apunta a un string"
	Content *string
}

func main() {
	content := "hola"

	/* 

	| Dirección | Valor |
	| --------- | ----- |
	| 0x000000  | h     |
	| 0x000001  | o     |
	| 0x000002  | l     |
	| 0x000003  | a     |
	| ...       | ...   |

	*/

	// Con el carácter '&' cogemos la *dirección de memoria* de la variable `content` y no su valor
	msg := Message{Content: &content}

	// Mirémos la memoria!
	/* 

	| Dirección | Valor    |
	| --------- | -----    |
	| 0x000000  | h        |
	| 0x000001  | o        |
	| 0x000002  | l        |
	| 0x000003  | a        |
	| 0x000004  | Message  |
	| 0x000005  | Content  |
	| 0x000006  | 0x000000 | <- Aquí guardamos la dirección de memoria!
	| ...       | ...      |

	*/

	// Ahora, hagamos pruebas! :)
	fmt.Println(content)
	// Aquí estamos "desreferenciando" la dirección, es decir, cogiendo el valor en vez de la
	// dirección de memoria
	fmt.Println(*msg.Content)
	fmt.Println(&content)
	fmt.Println(msg.Content)

	content = "qué tal? :)"

	fmt.Println(content)
	fmt.Println(*msg.Content)
	fmt.Println(&content)
	fmt.Println(msg.Content)
}
```

Ejemplos prácticos del uso de punteros y referencias en Go:

```go
package main

import (
	"encoding/json"
	"fmt"
)

type RespuestaAPI struct {
	Valor string `json:"valor"`
}

func main() {
	b := []byte(`{"valor": "HOLA"}`)

	rsp1 := RespuestaAPI{}
	// Aquí pasaremos `RespuestaAPI` como valor
	json.Unmarshal(b, rsp1)

	rsp2 := RespuestaAPI{}
	// Aquí pasaremos `RespuestaAPI` como dirección
	json.Unmarshal(b, &rsp2)

	fmt.Println(rsp1)
	fmt.Println(rsp2)

	// json.Umarshal modifica el contenido de RespuestaAPI, por eso si le pasamos una copia
	// y no la dirección de nuestra variable, no nos guarda el resultado
}
```

```go
package main

import "fmt"

type RespuestaAPI struct {
	Valor string `json:"valor"`
}

// Aquí `r` es `RespuestaAPI` (una copia). Cualquier cambio que hagamos dentro de la función
// `CambiarValor` se verá reflejado en la copia y no en la estructura inicial
func (r RespuestaAPI) CambiarValor(v string) {
	r.Valor = v
}

// Aquí `r` es un puntero hacia `RespuestaAPI`
func (r *RespuestaAPI) CambiarValorBueno(v string) {
	r.Valor = v
}

func main() {
	rsp := RespuestaAPI{
		Valor: "Hola!",
	}
	rsp.CambiarValor("Qué tal? :)")
	fmt.Println(rsp)

	rsp.CambiarValorBueno("Qué tal? :)")
	fmt.Println(rsp)
}
```

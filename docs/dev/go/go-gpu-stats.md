# Ejercicio: vamos a extraer estadísticas de GPUs

Nos interesa extraer estadísticas del uso de las GPUs para su posterior procesamiento. Las estadísticas que extraeremos serán las siguientes:

## Estadísticas por hypervisor

- Versión del driver de Nvidia
- Número total de tarjetas

## Estadísticas por tarjeta

- Nombre de la tarjeta
- UUID de la tarjeta
- PCI de la tarjeta
- Versión de firmware
- Perfil de performance
- Memoria total
- Memoria reservada por el driver
- Memoria usada
- Memoria libre
- Porcentaje de utilización de la GPU
- Porcentaje de utilización de la memoria de la GPU
- Temperatura
- Número de "particiones" de la tarjeta

Lo podemos hacer con las siguientes órdenes:

```sh
nvidia-smi --query-gpu="driver_version,count,name,uuid,pci.bus_id,vbios_version,pstate,memory.total,memory.reserved,memory.used,memory.free,utilization.gpu,utilization.memory,temperature.gpu" --format=csv,nounits
mdevctl list --dumpjson
```

Al lío!

<!-- `r.db('isard').table('vgpus').get('HYPER_ID-pci-PCI')` -->

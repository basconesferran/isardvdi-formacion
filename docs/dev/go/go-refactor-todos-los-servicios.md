# Refactor de los servicios de Go

Como hemos visto antes, hay algunos servicios que

1. O bien no se adhieren a la estructura de servicios de Go
2. O bien están en un repositorio externo, cosa que hace su mantenimiento más complejo

También hemos visto que aún se utiliza la dependencia antigua de `isardvdi-sdk-go`

Vamos a arreglarlo!

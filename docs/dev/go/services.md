## Go en IsardVDI

A día de hoy (15/06/23), hay 7 microservicios del proyecto que estén escritos en Go:

- `isard-authentication` -> Inicio de sesión
- `isard-guac` -> Visores RDP Web
- `isard-rdpgw` -> Visores RDP
- `isard-websockify` -> Visores Web
- `isard-stats-go` -> Extracción de estadísticas
- `isard-orchestrator` -> Autoescalado (aún no está en main!)
- `isard-check` -> Comprobación de que Isard y hypervisores funciona (aún no está en main!)

Se estructuran de la siguiente forma en el repositorio:

```sh
...
drwxr-xr-x authentication # Código del microservicio de inicio de sesión
drwxr-xr-x check          # Código del microservicio de comprobación de hypervisores y de IsardVDI
-rw-r--r-- go.mod         # Fichero de dependencias
-rw-r--r-- go.sum         # Hashes de las dependencias
drwxr-xr-x guac           # Código del microservicio de los visores RDP Web
drwxr-xr-x orchestrator   # Código del microservicio de autoescalado
drwxr-xr-x pkg            # Paquetes compartidos entre microservicios
drwxr-xr-x rdpgw          # Código del microservicio de los visores RDP
drwxr-xr-x stats          # Código del microservicio de estadísticas
drwxr-xr-x websockify     # Código del microservicio de los visores Web
```

Y los servicios se estructuran de la siguiente forma:

```sh
.
├── <servicio>
│   ├── <servicio>.go  # Implementación del servicio en sí
├── build
│   └── package
│       └── Dockerfile # Dockerfile del servicio
├── cfg
│   └── cfg.go         # Configuración del servicio (isardvdi.cfg)
├── cmd
│   └── <servicio>
│       └── main.go    # Paquete `main`
├── model
│   └── ...            # Definicion de los modelos de la DB
└── transport
    └── http           # Servidores para que puedan hablar con el servicio (por ejemplo: HTTP, gRPC)
```

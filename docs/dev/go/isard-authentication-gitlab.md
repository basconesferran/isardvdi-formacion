# Añadir un Provider a `isard-authentication` (GitLab)

Para añadir un provider a `isard-authentication`, tenemos que:

1. Identificar cómo podemos "autenticar" con el provider
2. Identificar cómo podemos extraer los datos de usuario (nombre, correo, etc)
3. Añadirlo! :)

En este caso, queremos añadir GitLab, así que vamos a investigar!

...

...

Encontramos que GitLab ofrece OAuth2! [https://docs.gitlab.com/ee/integration/oauth_provider.html](https://docs.gitlab.com/ee/integration/oauth_provider.html) y [https://docs.gitlab.com/ee/api/oauth2.html](https://docs.gitlab.com/ee/api/oauth2.html)

Y para el punto 2, tiene un endpoint que nos sirve! [https://docs.gitlab.com/ee/api/users.html#list-current-user](https://docs.gitlab.com/ee/api/users.html#list-current-user)

# `isardvdi-cli`

`isardvdi-cli` es el cliente oficial de IsardVDI para la línea de comandos. Por debajo, utiliza la librería `isardvdi-sdk-go`. Está un poco desfasado, así que vamos a actualizarlo para poder hacer scripts de bash!

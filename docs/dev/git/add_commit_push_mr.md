# Gestionar cambios

## Revisar cambios - git status y git diff

Una vez realizados los cambios sobre el código podemos ver que archivos se han modificado mediante el comando `git status`:

![Git status](../img/git/git_status.png)

Git explica los diferentes tipos de cambios que tenemos, en este caso tenemos dos: **not staged for commit** y **untracked files**.

* **Not staged for commit**. Cambios realizados sobre archivos que ya existían en el proyecto.
* **Untracked files**. Nuevos archivos creados en el proyecto de los que git aún no está haciendo seguimiento.

Podemos ver también qué ha cambiado mediante el comando `git diff`:

![Git diff](../img/git/git_diff.png)

## Añadir cambios - git add y git restore

Una vez revisados los cambios podemos añadirlos mediante el comando `git add ${ruta_fichero}`. Si queremos quitar algún archivo añadido lo podemos hacer mediante `git restore --staged ${ruta_fichero}`.


!!! Tip "Consejo"
    Antes de añadir los cambios pueden revisarse mediante el comando `git add -p`. Con este comando git mostrará los cambios realizados permitiendonos elegir que escoger y que descartar.

## Guardar cambios - git commit

Se pueden guardar los cambios añadidos con el comando `git commit`. Una vez introducido el comando git nos pedirá que pongamos una frase como resumen de los cambios. **Cada commit se identifica por un hash unívoco** como por ejemplo `896777fe6239c53026e446b5821a501735aa5cbe` al que nos solemos referir por sus primeros 8 dígitos.

!!! Tip "Consejo"
    Se puede definir el comentario del commit directamente mediante `git commit -m "${descripcion_cambios}"`.

!!! Danger "Formato del commit"
    En el caso de IsardVDI se sigue la escpecificación [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/#summary). Por ejemplo:

    `refactor(api): optimized list users endpoint`
    `feat(frontend,api): added create new desktop functionality`

## Publicar cambios - git push

Con los cambios guardados tendremos una versión del proyecto en local que será diferente a la que tenemos en la copia remota del proyecto, por lo que el siguente paso será actualizarlo con el comando `git push ${remote}`.

## Realizar una aportación - merge request

El estado en este punto se ve representado en el siguiente gráfico:

![Git mr](../img/git/isard_mr.png)

!!! Danger "Aclaración"
    No se podrá realizar cambios sobre el repositorio remoto oficial de manera directa, ya que este está protegido.

A continuación para incorporar los cambios en el repositorio oficial se creará una merge request. Se puede gestionar mediante la interfaz gráfica del fork de gitlab:

![Git mr1](../img/git/mr1.png)

![Git mr2](../img/git/mr2.png)

![Git mr3](../img/git/mr3.png)

Una vez creada la MR (merge request) será evaluada por el equipo de programadores, que evaluarán los cambios y dejarán comentarios en caso de que sea necesaria alguna modificación. Se pueden ver los comentarios, sugerencias y cambios mediante la interfaz gráfica:

* **Overview**. Historial de actividad sobre la MR y comentarios.

![Git mr overview](../img/git/mr_overview.png)

* **Commits**. Commits que incorpora la MR.

![Git mr commits](../img/git/mr_commits.png)

* **Changes**. Cambios incorporados en la MR.

![Git mr diff](../img/git/mr_diff.png)



Cuando se considere apta para su incorporación se aprobará (constancia de que todo está correcto) y se mergeará (integrará a main).

# Modificar commits

## Ver la historia de commits - git log

Mediante el comando `git log --graph` podemos ver de manera gráfica el historial de commits de la rama en la que estamos trabajando.

```
* commit 896777fe6239c53026e446b5821a501735aa5cbe (HEAD)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:46 2023 +0200
| 
|     feat(api): añadido archivo3
| 
* commit 892795b96b9002a091e4b9bc404e98e282ce615f
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:37 2023 +0200
| 
|     feat(api): añadido archivo2
| 
* commit e235fcfab28a540dcbf57a6d6add042c06e0ebf8 (rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 13:38:41 2023 +0200
| 
|     feat(api): añadido archivo1
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
| | 
| * commit 861bc11b09c8ab612a124925a59c4b0bfcc92408
| | Author: darta <josepmaria@isardvdi.com>
| | Date:   Tue May 9 18:39:33 2023 +0200
| | 
| |     fix(webapp): order hypervisors by id
| |   
* |   commit 1c028a5a3e9de903887e2adc8f46fbe77d5dedbb (tag: v10.68.1)
|\ \  Merge: 6c474c195 3b7bf3e4f
| | | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | | Date:   Tue May 9 18:43:52 2023 +0000
| | | 
| | |     Merge branch 'reset-destroy_time-on-orchestrator-manage' into 'main'
| | |     
| | |     fix(api): orchestrator managed endpoint reset destroy_time
| | |     
| | |     See merge request isard/isardvdi!2033
| | | 
| * | commit 3b7bf3e4f6d92d40c8ed9ed5f335337b0688779b
| | | Author: darta <josepmaria@isardvdi.com>
| | | Date:   Tue May 9 17:43:42 2023 +0200
| | | 
| | |     fix(api): orchestrator managed endpoint reset destroy_time
| | |   
1 2 3
```

En este ejemplo vemos lo siguiente:

* **Linea 1**. Estamos tres commits por encima de del repositorio oficial remoto (upstream).
* **Linea 2**. Se ha añadido el commit 3b7bf3e4 y se ha incorporado en la rama principal en el commit 1c028a5a (v10.68.1).
* **Linea 3**. Se ha añadido el commit 861bc11b y se ha incorporado en la rama principal en el commit 50ba96ed (v10.68.2).

## Cambiar los últimos commits - git rebase -i HEAD~${numero}

Podemos modificar uno o más commit más antiguos que el último guardado mediante el comando `git rebase -i HEAD~${numero}`, dónde ${numero} es la cantidad de **commits hacia atrás** que queremos gestionar. Considerando este caso:

```
* commit 896777fe6239c53026e446b5821a501735aa5cbe (HEAD)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:46 2023 +0200
| 
|     feat(api): añadido archivo3
| 
* commit 892795b96b9002a091e4b9bc404e98e282ce615f
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:37 2023 +0200
| 
|     feat(api): añadido archivo2
| 
* commit e235fcfab28a540dcbf57a6d6add042c06e0ebf8 (rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 13:38:41 2023 +0200
| 
|     feat(api): añadido archivo1
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
```

Podriamos modificar los 3 últimos commits modificando *pick* por una de los comandos explicados:

![Git rebase interactive](../img/git/git_rebase_interactive.png)

**Reword**

Cambiar el mensaje del commit. Debemos cambiar el comando pick por `reword`o `r`:

```
pick e235fcfab feat(api): añadido archivo1
reword 892795b96 feat(api): añadido archivo2
pick 896777fe6 feat(api): añadido archivo3
```

Y guardar los cambios. A continuación git mostrará el mensaje a modificar y al guardar se modificará la historia:

![Reword](../img/git/reword.png)

**Drop**

Eliminar commit. Debemos cambiar el comando pick por `drop` o `d`:

```
pick e235fcfab feat(api): añadido archivo1
drop 892795b96 feat(api): añadido archivo2
pick 896777fe6 feat(api): añadido archivo3
```

Y al guardar los cambios se modificará la historia:

![Drop](../img/git/drop.png)

**Squash**


Unificar dos commits. Debemos cambiar el comando pick por `squash` o `s`:

```
pick e235fcfab feat(api): añadido archivo1
squash 892795b96 feat(api): añadido archivo2
pick 896777fe6 feat(api): añadido archivo3
```

Y guardar los cambios. A continuación git mostrará los mensajes de los commits a mezclar por si se quiere realizar alguna modificación y al guardar se modificará la historia:

![Squash](../img/git/squash.png)

**Fixup**


Unificar dos commits quedándose con el mensaje del commit anterior (o el actual con -C o -c). Debemos cambiar el comando pick por `fixup` o `f`:

```
pick e235fcfab feat(api): añadido archivo1
fixup 892795b96 feat(api): añadido archivo2
pick 896777fe6 feat(api): añadido archivo3
```

Y al guardar los cambios se modificará la historia:

![Fixup](../img/git/fixup.png)


## Reubicar los commits - git cherry-pick

Podemos modificar la rama de los commits mediante el comando cherry-pick.

**Ejemplo**

Supongamos el caso que hemos creado tres archivos independientes cuyo propósito es muy diferente. El archivo 1 y el archivo 2 están listos para utilizarse en producción, pero el archivo 3 aún necesita modificaciones. El archivo 3 resultaría bloqueante por lo que sería mejor gestionarlo en una [merge request](../git/add_commit_push_mr.md#realizar-una-aportación---merge-request) aparte. De hecho, si son tres archivos independientes y tienen distintas finalidades, lo ideal sería crear 3 MRs diferentes. Partiendo de esta base (3 commits en la rama_1):

```
* commit 896777fe6239c53026e446b5821a501735aa5cbe (HEAD) -> rama_1
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:46 2023 +0200
| 
|     feat(api): añadido archivo3
| 
* commit 892795b96b9002a091e4b9bc404e98e282ce615f
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:37 2023 +0200
| 
|     feat(api): añadido archivo2
| 
* commit e235fcfab28a540dcbf57a6d6add042c06e0ebf8 (rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 13:38:41 2023 +0200
| 
|     feat(api): añadido archivo1
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
| | 
```

Podemos crear una rama para los commits del archivo 2:

`git switch -C rama_2 upstream/main`

Y llevarnos el commit:

`git cherry-pick 892795b9`

Si vemos la historia de la rama 2 será la siguiente:

```
* commit fd398aa9bd917220f1a2707e45a8c0b3bf423088 (HEAD -> rama_2)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:37 2023 +0200
| 
|     feat(api): añadido archivo2
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
| | 
```

!!! Warning "Atención"
    Los commits se identifican por hashes y al hacer cherry-pick se modifica el hash.


Podemos replicar lo mismo para los commits del archivo 3:

`git switch -C rama_3 upstream/main`

Y llevarnos el commit:

`git cherry-pick 896777fe`

Si vemos la historia de la rama 3 será la siguiente:

```
* commit 29147282733f88a01ea6ee9be17c06a8c375597d (HEAD -> rama_3)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:46 2023 +0200
| 
|     feat(api): añadido archivo3
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
| | 
```

Y finalmente eliminar ambos commits de rama_1, para que sólo le queden los cambios sobre el archivo1. Cambiamos los últimos dos commits:

`git rebase -i HEAD~2` 

Y los eliminamos:

```
pick e235fcfab feat(api): añadido archivo1
drop 892795b96 feat(api): añadido archivo2
drop 896777fe6 feat(api): añadido archivo3
```

Si vemos la historia de rama_1 será la siguiente:

```
* commit e235fcfab28a540dcbf57a6d6add042c06e0ebf8 (HEAD -> rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 13:38:41 2023 +0200
| 
|     feat(api): añadido archivo1
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
```

## Resolver conflictos

Frecuentemente se realizan cambios sobre un mismo archivo en paralelo, generando así conflictos que hay que resolver. Supongamos que dos programadores 'Programador A' y 'Programador B' trabajan sobre la misma rama 'feature_1'. 'Programador A' ha integrado cambios en feature_1, por lo que 'Programador B' [decide actualizar la rama local](../git/branch_clone_fetch_pull.md#mantener-el-repositorio-actualizado-con-git-pull-o-git-rebase) en la que está trabajando. Ambos han modificado el mismo archivo: 'feature_1.py' y git nos informará del conflicto, haciendo escoger a 'Programador  B' una solución.

Estado de feature_1 (rama de trabajo compartida):

```
* commit 74367581e680bd468458cedf621c527b6a22c06d (HEAD -> feature_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:12:50 2023 +0200
| 
|     feat(api): feature 1 prints
| 
* commit 1aaab7aeb3515e77f8f863614d913f44b149b3cd
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 17:59:54 2023 +0200
| 
|     feat(api): feature 1
|   
```

Estado de feature_1_programador_B (rama de trabajo de 'Programador B'):

```
* commit d4ad2f80a10f3326118832406e1b2b4d450c1b26 (HEAD -> feature_1_programador_B)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:15:57 2023 +0200
| 
|     feat(api): añadido un print a feature_1.py
| 
* commit 1aaab7aeb3515e77f8f863614d913f44b149b3cd
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 17:59:54 2023 +0200
| 
|     feat(api): feature 1
|   

```

'Programador B' deberá incorporar el commit 74367581 perteneciente a 'Programador A' y tiene como objetivo obtener el siguiente histórico:

```
* commit d4ad2f80a10f3326118832406e1b2b4d450c1b26 (HEAD -> feature_1_programador_B)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:15:57 2023 +0200
| 
|     feat(api): añadido un print a feature_1.py
| 
* commit 74367581e680bd468458cedf621c527b6a22c06d (HEAD -> feature_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:12:50 2023 +0200
| 
|     feat(api): feature 1 prints
| 
* commit 1aaab7aeb3515e77f8f863614d913f44b149b3cd
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 17:59:54 2023 +0200
| 
|     feat(api): feature 1
|   
```

Pero han modificado el mismo archivo y git lo detectará:

```
$ git rebase feature_1

Auto-merging feature_1.py
CONFLICT (content): Merge conflict in feature_1.py
error: could not apply d4ad2f80a... feat(api): añadido un print a feature_1.py
hint: Resolve all conflicts manually, mark them as resolved with
hint: "git add/rm <conflicted_files>", then run "git rebase --continue".
hint: You can instead skip this commit: run "git rebase --skip".
hint: To abort and get back to the state before "git rebase", run "git rebase --abort".
Could not apply d4ad2f80a... feat(api): añadido un print a feature_1.py
```

Podemos ver que git avisa que no ha podido aplicar el commit de 'Programador B' (d4ad2f80a). En vscode se mostrará de la siguiente forma:

![Conflicto](../img/git/conflict.png)

Vemos que se refiere como `<<<<<<< HEAD (Current Change)` a los cambios de 'Programador A' y como `>>>>>>> d4ad2f80a (feat(api): añadido un print a feature_1.py) (Incoming Change)` a los cambios de 'Programador B'. Es así porque en el momento del conflicto la historia se encuentra de la siguiente forma:

```
* commit 74367581e680bd468458cedf621c527b6a22c06d (HEAD, feature_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:12:50 2023 +0200
| 
|     feat(api): feature 1 prints
| 
* commit 1aaab7aeb3515e77f8f863614d913f44b149b3cd
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 17:59:54 2023 +0200
| 
|     feat(api): feature 1
|   
```

Y hace decidir a 'Programador B' cual es el estado final que quiere para el archivo con conflicto en su commit (d4ad2f80a). Tiene 3 opciones:

* **Quedarse los cambios de 'Programador A'**

Historial de commits:

```
* commit 74367581e680bd468458cedf621c527b6a22c06d (HEAD, feature_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:12:50 2023 +0200
| 
|     feat(api): feature 1 prints
| 
* commit 1aaab7aeb3515e77f8f863614d913f44b149b3cd
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 17:59:54 2023 +0200
| 
|     feat(api): feature 1
|   
```
Estado final del archivo:

```
print("Holi")
```

* **Quedarse los cambios de 'Programador B'**

Historial de commits:

```
* commit d4ad2f80a10f3326118832406e1b2b4d450c1b26 (HEAD -> feature_1_programador_B)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:15:57 2023 +0200
| 
|     feat(api): añadido un print a feature_1.py
| 
* commit 74367581e680bd468458cedf621c527b6a22c06d (HEAD -> feature_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 18:12:50 2023 +0200
| 
|     feat(api): feature 1 prints
| 
* commit 1aaab7aeb3515e77f8f863614d913f44b149b3cd
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 17:59:54 2023 +0200
| 
|     feat(api): feature 1
|   
```

Estado final del archivo:

```
print("Hola")
```

* **Modificar el código a su gusto** (en este caso se descarta el commit d4ad2f80a y se genera uno nuevo):

```
commit 6f7efc928122c3c1970fe00710a8fc378e2cd75e (HEAD -> feature_1_programador_B)
Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
Date:   Wed May 10 18:15:57 2023 +0200

    feat(api): añadido un print a feature_1.py

commit 74367581e680bd468458cedf621c527b6a22c06d (feature_1)
Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
Date:   Wed May 10 18:12:50 2023 +0200

    feat(api): feature 1 prints

commit 1aaab7aeb3515e77f8f863614d913f44b149b3cd
Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
Date:   Wed May 10 17:59:54 2023 +0200

    feat(api): feature 1
```

Estado final del archivo:

```
print("Holi")
print("Hola")
```




## Descartar commits - git reset --hard y git reset --soft HEAD~${numero}

!!! Danger "Cuidado"
    Aunque es posible recuperar los cambios realizados por la siguientes acciones requieren conocimientos más avanzados de git. Pueden conllevar la perdida de cambios y código.

**Soft**

Supongamos que tenemos un commit en el que hemos añadido un archivo 'archivo_1' con el siguiente historial:

```
* commit e235fcfab28a540dcbf57a6d6add042c06e0ebf8 (HEAD -> rama_1, origin/rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 13:38:41 2023 +0200
| 
|     feat(api): añadido archivo1
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
| | 
```

Podemos deshacer el último commit mediante el comando `git reset --soft HEAD~1`.

```
$ git reset --soft HEAD~1
$ git status

On branch rama_1
Your branch is up to date with 'upstream/main'.

Changes to be committed:
  (use "git restore --staged <file>..." to unstage)
	new file:   archivo1.txt

```

**Hard**

`git reset --hard ${commit_hash}` se utiliza para eliminar todos los cambios de la rama actual y deja la rama en el estado del commit especificado.

## Reescribir la historia - git push -f

!!! Danger "Cuidado"
    Forzar la actualización del repositorio remoto es peligroso (puede afectar al resto del equipo), por lo que es aconsejable evitarlo.

    Aunque es posible recuperar los cambios sobreescritos por `git push -f ${remote}` se requieren conocimientos más avanzados de git. Puede conllevar la perdida de cambios y código.

En git se prioriza el repositorio remoto sobre el local, por este motivo el comando `git push` está limitado a **solo añadir cambios después de haber incorporado lo que haya en el remoto**. De esta manera se respetan los cambios de el resto, aunque es posible reescribir el historial de commits. Se puede conseguir mediante el comando `git push -f`. Por ejemplo:

```
* commit 896777fe6239c53026e446b5821a501735aa5cbe (HEAD, origin/rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:46 2023 +0200
| 
|     feat(api): añadido archivo3
| 
* commit 892795b96b9002a091e4b9bc404e98e282ce615f
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 15:46:37 2023 +0200
| 
|     feat(api): añadido archivo2
| 
* commit e235fcfab28a540dcbf57a6d6add042c06e0ebf8 (rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 13:38:41 2023 +0200
| 
|     feat(api): añadido archivo1
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
| | 
```

En este caso vemos que rama_1 (rama local) difiere de rama_1 de origin (rama remota), podemos forzar a la rama remota a que sea igual que la rama local **pero debemos estar seguros**. Mediante `git push -f origin` podemos forzar la actualización:

```
* commit e235fcfab28a540dcbf57a6d6add042c06e0ebf8 (HEAD -> rama_1, origin/rama_1)
| Author: Miriam Melina Gamboa Valdez <melina.devel@gmail.com>
| Date:   Wed May 10 13:38:41 2023 +0200
| 
|     feat(api): añadido archivo1
|   
*   commit 50ba96edaa34edd04dcd848b995ae9d7aa63f915 (tag: v10.68.2, upstream/main, upstream/HEAD, main)
|\  Merge: 1c028a5a3 861bc11b0
| | Author: Josep Maria Viñolas Auquer <josepmaria@isardvdi.com>
| | Date:   Tue May 9 19:39:02 2023 +0000
| | 
| |     Merge branch 'order-hypers-by-id' into 'main'
| |     
| |     fix(webapp): order hypervisors by id
| |     
| |     See merge request isard/isardvdi!2034
| | 
```
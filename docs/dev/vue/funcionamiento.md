# Cómo funciona frontend

Juntando todo lo visto hasta ahora el esquema de funcionamiento seria similar al siguiente:

![Funcionamiento frontend](../img/vue/funcionamiento_frontend.png)

1. Cuando el usuario se dirige a una ruta el router de vue devuelve el Layout correspondiente (la mayoría de veces el MainLayout, que incorpora la barra de navegación) y la página a renderizar. Esta página está compuesta por diferentes componentes.

2. Cada componente se comunica con la store de Vuex, que es la encargada de pedir la información necesaria a la API mediante una action. En recibir la respuesta de la API esta se procesa con el util correspondiente, que formatará la información y la guardaremos en la store a través de una mutation.

3. Una vez guardada en la store podemos acceder mediante getters a la información recibida de la api desde el componente que queramos.


## Vuex store y utils

La store de Vuex es el lugar donde guardamos la información que está visualizando el usuario, el estado de la aplicación.

Conceptos base:

* State

Vuex utiliza un único árbol de estado, es decir, este único objeto contiene todo el estado de su aplicación y sirve como la "única fuente de verdad". Podemos referirnos al state de la store desde cada componente mediante el objeto $store:

```
const Counter = {
  template: `<div>{{ count }}</div>`,
  computed: {
    count () {
      return this.$store.state.count
    }
  }
}
```

Mas información en [https://vuex.vuejs.org/guide/state.html](https://vuex.vuejs.org/guide/state.html)

* Getters

Aunque podemos acceder al state mediante el objeto $store lo más recomendable es definir getters, que se encargan de devolvernos la información o computarla en caso de necesidad (filtrando por ejemplo).

Más información en [https://vuex.vuejs.org/guide/getters.html](https://vuex.vuejs.org/guide/getters.html)

* Mutations

Permiten guardar la información en el árbol de estado de la aplicación.

Más información en [https://vuex.vuejs.org/guide/mutations.html](https://vuex.vuejs.org/guide/mutations.html)

* Actions

Las acciones son las encargadas de la comunicación con la API y de la ejecución de mutations.

Más información en [https://vuex.vuejs.org/guide/actions.html](https://vuex.vuejs.org/guide/actions.html)

* Modules

Debido al uso de un único árbol de estado, todos los estados de la aplicación están contenidos dentro de un gran objeto. Sin embargo, a medida que nuestra aplicación escala, la store puede resultar difícil de manejar.

Para ayudar con eso, Vuex nos permite dividir nuestra tienda en módulos. Cada módulo puede contener su propio estado, mutaciones, acciones y getters. En el caso de IsardVDI por ejemplo en vez de tener todo en un único archivo lo hemos desglosado en varios módulos para facilitar su manutención:

```
modules: {
    auth,
    templates,
    template,
    desktops,
    domain,
    deployments,
    deployment,
    config,
    vpn,
    sockets,
    allowed,
    booking,
    snotify,
    planning,
    profile,
    media,
    storage
  }
```


## Ejemplo profile

Veamos ahora un ejemplo concreto: la página de profile del usuario.


* El router redirige a la ruta /profile:

```
frontend/src/router/index.js

{
  path: '/profile',
  component: MainLayout,
  children: [
    {
      path: '',
      name: 'profile',
      component: Profile,
      meta: {
        title: i18n.t('router.titles.profile'),
        allowedRoles: ['admin', 'manager', 'advanced', 'user']
      }
    }
  ],
  meta: {
    requiresAuth: true
  }
},
```

* El componente pide información a la api mediante la action "fetchProfile"

```
frontend/src/pages/Profile.vue

<script>
.
.
.
export default {
  components: {
    ProfileCardSkeleton,
    Language,
    PasswordModal,
    QuotaProgressBar
  },
  // Ejecución al crear la página
  setup (_, context) {
    const $store = context.root.$store

    $store.dispatch('fetchProfile') // Llamamos a la action fetchProfile de la store
    const profile = computed(() => $store.getters.getProfile)
    const profileLoaded = computed(() => $store.getters.getProfileLoaded)

    return { profile, profileLoaded }
  },
  destroyed () {
    this.$store.dispatch('resetProfileState')
  },
  methods: {
    ...mapActions([
      'showPasswordModal'
    ])
  }
}
</script>

```

* La action "fetchProfile" llama a la api

```
frontend/src/store/modules/profile.js

actions: {
    resetProfileState (context) {
      context.commit('resetProfileState')
    },
    fetchProfile (context) {
      axios
        .get(`${apiV3Segment}/user`) // Llamamos al endpoint /api/v3/user
        .then(response => {
          context.commit('setProfile', ProfileUtils.parseProfile(response.data))
        })
        .catch(e => {
          ErrorUtils.handleErrors(e, this._vm.$snotify)
        })
    },
```

* En recibir la respuesta la "parsea" mediante la función parseProfile (definimos que campos cogemos de la respuesta y los procesamos y adaptamos en caso que sea necesario) y la guardamos a través de la mutation llamada "setProfile":

```
frontend/src/store/modules/profile.js

actions: {
    resetProfileState (context) {
      context.commit('resetProfileState')
    },
    fetchProfile (context) {
      axios
        .get(`${apiV3Segment}/user`)
        .then(response => {
          context.commit('setProfile', ProfileUtils.parseProfile(response.data)) // Parse la respuesta
        })
        .catch(e => {
          ErrorUtils.handleErrors(e, this._vm.$snotify)
        })
    },
```

```
frontend/src/utils/profileUtils.js

export class ProfileUtils {
  static parseProfile (item) {
    const { category_name: category, email, group_name: group, name, provider, quota, used, restriction_applied: restrictionApplied, role_name: role, username, photo, secondary_groups: secondaryGroups, total_disk_size: totalDiskSize } = item
    return {
      category,
      email,
      group,
      name,
      provider,
      used: used ? this.parseQuota(used) : false,
      quota: quota ? this.parseQuota(quota) : false,
      restrictionApplied,
      role,
      username,
      photo,
      secondaryGroups: secondaryGroups.length > 0 ? this.parseSecondaryGroups(secondaryGroups) : '-',
      totalDiskSize
    }
  }
```

```
frontend/src/store/modules/profile.js

setProfile (state, profile) {
    state.profile = profile
    state.profile_loaded = true
},
```
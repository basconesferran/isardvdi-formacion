# Directivas básicas de VueJS

* {{ expresión }} - Mostrar expresiones de JavaScript dentro de templates
* [Enlazar un atributo a una variable](https://es.vuejs.org/v2/api/#v-bind) v-bind:atributo="expresion" ó :atributo="expresion"
* [Renderizado condicional](https://vuejs.org/guide/essentials/conditional.html)
    * v-if -> Renderizar un elemento si se cumple una condición
    * v-else-if -> Si no se ha cumplido una condición en un v-if renderizar en base a otra condición 
* [Iteración](https://es.vuejs.org/v2/guide/list.html) v-for -> Renderizar un elemento repetidas veces
* [Manejo de eventos](https://es.vuejs.org/v2/guide/events.html) v-on ó @
* [Computed properties](https://v2.vuejs.org/v2/guide/computed.html) -> Evitar lógica en la template para mantenerla simple

[Playground de vue](https://play.vuejs.org)

<a href="../../files/vue/example1.vue" download>Ejemplo 1 playground: Directivas básicas en vue</a>

<a href="../../files/vue/example2.vue" download>Ejemplo 2 playground: Incorporar un child y enviarle información</a>

<a href="../../files/vue/example3.vue" download>Ejemplo 3 playground: Definir métodos y procesar la información</a>

<a href="../../files/vue/example4.vue" download>Ejemplo 4 playground: Variables calculadas mediante computed properties</a>

# Más ejemplos

Ejemplo de funcionamiento de un component: [https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Vue_first_component](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Vue_first_component)

En el ejemplo adjunto vemos que se realizan los siguientes pasos:

1. [Crea un componente ToDoItem](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Vue_first_component#creating_a_todoitem_component)
2. [Incorpora ToDoItem en la página (App.vue).](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Vue_first_component#using_todoitem_inside_our_app)  En script lo importa y en el html lo referencia. Vemos que se muestra el componente dentro del componente principal (en su caso App.vue). Llegados a este punto podemos ver que el componente no es muy útil, ya que simplemente nos muestra siempre el mismo item (My Todo Item), por lo que vamos a darle una mayor flexibilidad mediante lo que llamaremos props.
3. [Añade propiedades al componente.](https://developer.mozilla.org/en-US/docs/Learn/Tools_and_testing/Client-side_JavaScript_frameworks/Vue_first_component#making_components_dynamic_with_props) entre los tags `<script>` añade la definición de las propiedades que tendrá el componente.

![Ejemplo de props](../img/vue/props_example.png)

Vemos que el componente ToDoItem espera dos propiedades: label y done. Label será el texto que mostraremos al lado del checkbox y done será si está marcado o no la caja de selección. Se genera también un identificador unívoco para cada "todo item", sino sólo podríamos tener un único item, ya que generaría conflictos.

En el caso de IsardVDI pages y views son simplemente componentes (igual que los que encontramos bajo el directorio components), que nos permiten tener una "página principal" a la que referirnos, que contendrá otros componentes.

Veamos por ejemplo el fichero [Profile.vue](https://gitlab.com/isard/isardvdi/-/blob/main/frontend/src/pages/Profile.vue). Vemos que tiene el [skeleton](https://gitlab.com/isard/isardvdi/-/blob/main/frontend/src/pages/Profile.vue#L29) (página de carga), [modal de contraseña](https://gitlab.com/isard/isardvdi/-/blob/main/frontend/src/pages/Profile.vue#L54), [idioma](https://gitlab.com/isard/isardvdi/-/blob/main/frontend/src/pages/Profile.vue#L168), etc.

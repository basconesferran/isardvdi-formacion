# Introducción al desarrollo

Los siguientes apartados están enfocados al desarrollo en las tecnologías utilizadas en IsardVDI:

* [**Git**](./git/intro.md): Utilizado para gestionar el historial de cambios sobre el código.

* [**Go**](./go/intro.md): Lenguaje de programación utilizado en el microservicio de autenticación (encargado del login y registro de usuarios) isard-authentication.

* [**Python**](./python/auto.md): Lenguaje de programación utilizado en varios microservicios tales como isard-engine, isard-api y isard-webapp.

* [**Flask**](./flask/ajax.md): Framework del lenguaje de programación Python utilizado en el panel de administración (isard-webapp) y en el microservicio de API de datos (isard-api).

* [**Vue**](./vue/router.md): Framework del lenguaje de programación Javascript utilizado en el apartado de usuario.
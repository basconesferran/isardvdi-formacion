# Ejercicio: Crear panel Grafana

## Introducción

En este ejercicio se creará un nuevo panel para Grafana y se añadirán diferentes paneles a gusto de cada uno y aprender a utilizar lo básico de Gitlab.

## Preparación 

Se realizará este ejercicio con un Grafana, en este caso podría ser el de un Isard limpio y posteriormente subir a Gitlab los cambios realizados.

!!! Tip "Requisitos"

    - **Cuenta en gitlab**: <https://gitlab.com/users/sign_in>

    - **Crear un nuevo fork de Isard para poder subir los cambios**: <https://gitlab.com/isard/isardvdi/-/forks/new>

    - **Clonar Isard limpio del fork**: `git clone https://gitlab.com/vuestro/fork.git`

    - **Copiar el *isardvdi.cfg.exampl*e* a *isardvdi.cfg* con el Flavour all-in-one o monitor.**

    - **Hacer `./build.sh` y arrancar el isard `docker-compose up -d` o `docker compose up -d` dependiendo de la versión del docker compose.**

## Realización

1.- **Accede al apartado de monitor de tu máquina, ejemplo: `localhost/monitor`, para crear el nuevo panel hay que ir al apartado `Dashboards` y en `New dashboard` se creará.**

2.- **Seleccionar uno por uno los paneles que queramos copiar a este nuevo Dashboard o crear nuevos.**

!!! Danger "Recomendación"
    Tener dos ventanas abiertas para ir copiando los paneles y sin recargar la página del nuevo Dashboard, porque sino se perderán los cambios. 

![](https://i.imgur.com/eCnpo30.png)

3.- **Pegar lo copiado en el nuevo Dashboard.**

![](https://i.imgur.com/QUH0aoN.png)

4.- **Una vez tengamos los paneles en el nuevo Dashboard copiaremos el JSON y lo guardaremos en un nuevo archivo .json, este archivo puede ir al directorio de dashboards que son personalizados para una instalación `/isard/monitor/grafana/custom/`o se puede añadir diretamente en `/docker/grafana/dashboards/` que serán los paneles por defecto que cargarán al arrancar el Grafana**

Lanzar después de copiar el fichero en el directorio:

```
./build.sh
```

5.- **Probar que arranca correctamente el Grafana con el nuevo panel.**

Para arrancar con los cambios:

!!! Warning "Importante"
    Estos cambios no los pillará si no hacemos build del contenedor y se recrea con estos cambios, para esto es necesario cambiar la línea `USAGE=production` a `USAGE=build` en el isardvdi.cfg sino al intentar hacer el build saltará el error: `isard-grafana uses an image, skipping`.

```
docker-compose build isard-grafana
docker-compose up -d isard-grafana
```

Y comprobar nuevamente en monitor `localhost/monitor` que el nuevo panel aparece.

6.- **Para acabar, subiremos los cambios al respositorio de Gitlab para dejarlos de forma permanente.**

Primero se lanza un git status para revisar que cambios vamos a subir al repositorio de Gitlab, después con git add añadimos los cambios que hemos hecho, con git commit -am 'título del commit' crearemos el commit que se subirá con un título relacionado con los cambios realizados y por último lanzamos un git push para subir el commit.

```
git status
git add (de los cambios)
git commit -am 'ejercicio: nuevo panel añadido'
git push
```

# isard-storage

El contenedor isard-storage es el único del sistema que está basado en una imágen que no es Alpine. Su imágen base es Fedora [https://gitlab.com/isard/isardvdi/-/tree/main/docker/storage](https://gitlab.com/isard/isardvdi/-/tree/main/docker/storage)

Esto es debido a que Fedora es una distribución basada en RedHat, y que usa RedHat para sus paquetes para testearlos antes de incorporarlos. RedHat es una de las distribuciones más activas en cuanto al desarrollo en virtualización [https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/index](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/virtualization_deployment_and_administration_guide/index) y ese es el motivo de usar Fedora en nuestro contenedor.

Este es el contenedor en el que realizaremos los ejercicios de almacenamiento virtual.
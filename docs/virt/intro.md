# Virtualización

Entendemos como virtualización la emulación de todo el *hardware* de un ordenador: placa base, cpu, ram, disco, tarjeta gráfica,...

Las máquinas virtuales nos permitirán tener diferentes sistemas operativos ejecutándose sobre el mismo hardware.

## Máquina real (S.O. huésped) y máquina virtual (S.O. invitado)

- **Máquina real**: Es el hardware físico que tenemos, con todos los dispositivos físicos (*cpu, memoria, interfaces...)
- **Sistema operativo huésped (host)**: Es el sistema operativo que tendremos instalado en el hardware real y que nos permitirá ejecutar aplicaciones y procesos. Es el encargado de gestionar la máquina real.
- **Máquina virtual**: Se trata de un software (aplicación) que tiene como finalidad simular la existencia de todo un hardware programado. Por lo tanto este hardware es como el hardware físico, pero está programado como una aplicación. Esto nos permitirá instalar un nuevo sistema operativo, aislado del host.
- **Sistema operativo invitado (guest)**: Es un sistema operativo como cualquier otro pero que hemos instalado en la aplicación de virtualización que nos ha simulado una máquina real. Por lo tanto este sistema operativo invitado se ejecuta como una aplicación más para el sistema operativo huésped.

## Virtualización

Existen varios sistemas de virtualización (software que permiten simular un hardware, que será virtual y que denominaremos hipervisores*):

- [LXC](https://en.wikipedia.org/wiki/lxc) – lightweight Linux *container *system
- [OpenVZ](https://en.wikipedia.org/wiki/openvz) – lightweight Linux *container *system
- [Kernel-*based Virtual *Machine](https://en.wikipedia.org/wiki/kernel-based_virtual_machine)/[QEMU](https://en.wikipedia.org/wiki/qemu) (KVM) – open-source hypervisor for Linux and SmartOS
- [Xen](https://en.wikipedia.org/wiki/xen) – Bare-Metal hypervisor
- [User-modo Linux](https://en.wikipedia.org/wiki/user-mode_linux) (UML) paravirtualized kernel
- [VMware *ESXi](https://en.wikipedia.org/wiki/vmware_esxi) and GSX – hypervisors for Intel hardware
- [Hyper-V](https://en.wikipedia.org/wiki/hyper-v) – hypervisor for Windows by Microsoft
- [PowerVM](https://en.wikipedia.org/wiki/powervm) – hypervisor by IBM for [AIX](https://en.wikipedia.org/wiki/ibm_aix), Linux and IBM y
- [Parallels *Workstation](https://en.wikipedia.org/wiki/parallels_workstation) – hypervisor for Mac by Parallels IP Holdings GmbH
- [Bhyve](https://en.wikipedia.org/wiki/bhyve) – hypervisor for [FreeBSD](https://en.wikipedia.org/wiki/freebsd) 10

Estos hypervisores llevan asociado algún tipo de software que permite gestionar la creación y events asociados a las máquinas virtuales:

- *libvirt + virt-manager*: Es una de las soluciones que permite gestionarlos todos desde *GUI*
- *libvirt + virsh*: El mismo que *virt-manager* pero desde CLI
- *oVirt*: Interfaz web para gestionar máquinas virtuales en hypervisors KVM/*qemu
- *VMware Workstation y VMware Player: Gestión de hipervisors *VMware
- *VirtualBox*: Gestión de hypervisors de Oracle
- *IsardVDI*: Gestión centralizada de hipervisores KVM/qemu
- *Proxmox*: Gestión de contenedores ligeros (LXC, OpenVZ) y de hypervisors KVM/qemu
- ...
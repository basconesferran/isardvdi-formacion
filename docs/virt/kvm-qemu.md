# KVM/qemu

## Creación de máquinas virtuales

Para crear máquinas virtuales haremos uso de *KVM/qemu* puesto que es software libre y gratuito, basándonos en una distribución cualquiera como sistema operativo huésped.

Qemu es capaz de emular procesadores y por lo tanto de virtualizar sin KVM. KVM es un módulo de Linux que permite explotar las extensiones hardware de nuestra CPU para proporcionar un mejor rendimiento en la virtualización.

Más información: [https://i2ds.org/2015/06/25/virtualizacion-a-fondo-la-relacion-entre-kvm-y-qemu/](https://i2ds.org/2015/06/25/virtualizacion-a-fondo-la-relacion-entre-kvm-y-qemu/)

```
COMPROBACIÓN PREVIA
Hace falta que el sistema operativo huésped tenga acceso a la virtualización de hardware. Esta opción depende del procesador físico ( Intel VT o AMD-V) que tenga la máquina real y habitualmente se puede activar esta funcionalidad de virtualización desde la *BIOS.

Con el comando: 

egrep '(vmx|svm)' /proc/cpuinfo

podemos comprobar si nuestro procesador permite esta funcionalidad de virtualización y se encuentra activada a la BIOS. Veremos una salida como la siguiente en caso de que la tenga activa:

flags		: fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc aperfmperf pni pclmulqdq dtes64 monitor ds_cpl **vmx** **smx** est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 popcnt aes lahf_lm kaiser tpr_shadow vnmi flexpriority ept vpid dtherm ida arat

```

### Creación manual de una máquina virtual (qemu)

El arranque de una máquina virtual se hará con la hipervisor qemu que tendremos que tener instalado. Antes que nada arrancaremos una sencilla máquina desde línea de comandos.

1. **Paquetes a instalar**: 

    - RedHat/CentOS/Fedora: *dnf install qemu-kvm*

    - Debian/Ubuntu: *sudo apt install qemu-kvm virt-manager virtinst libvirt-clients bridge-utils libvirt-daemon-system -y*

2. **Descargar una máquina virtual de prueba**: Iremos al web [http://www.qemu-advent-calendar.org/2016/](http://www.qemu-advent-calendar.org/2016/) donde encontramos divertidas máquinas antiguas ya instaladas en un fichero de disco duro. Si clicamos a la puerta *nº10* encontramos el 'Epic Pinball Demo' que descargaremos. Dentro encontraremos:
 1. *freedos2016.qcow2*: Este es el disco duro que queremos arrancar con nuestra máquina virtual. Las máquinas virtuales ven los discos duros como reales pero en realidad no son más que ficheros como estos de donde leen y escriben los datos.
 2. run.sh: Código *bash que al ejecutarse nos creará y arrancará un proceso 


3. **Arrancar la máquina virtual**: Podríamos usar el run.sh pero lo haremos con el comando que realmente usa el run.sh:

    *qemu-system-x86_64 freedos2016.qcow2 -soundhw sb16 \
   ​    -vga std,retrace=precise -display sdl*

    donde si lo desglosamos veremos que:

    - **qemu-system-x86_64**: Es el hipervisor que simulará procesadores y arquitecturas de 64 bits (compatibles con las de 32 bits).
    - **freedos2016.qcow2**: Este primer parámetro le dice cuál es el fichero que tiene que tomar como disco la máquina virtual que crearemos.
    - **soundhw sb16**: Simulamos una tarjeta de sonido modelo soundblaster16 (de la época del juego que queremos arrancar)
    - **vga, std,...**: Simulamos una tarjeta gráfica estándar vga (resolución máxima de 640x480 de la época del juego)
    - display sdl: Simulamos una pantalla genérica

4. **Juguemos!**: Si lo hemos hecho bien ahora se nos abrirá una ventana (pantalla) de la máquina virtual que acabamos de arrancar y empezará el juego de *pinball. En realidad se trata de un juego hecho para el sistema operativo MS-DOS que es realmente el sistema operativo que estamos arrancando.

Ahora que hemos visto como crear una máquina con hipervisor qemu podéis explorar las diferentes opciones de procesador, pantalla, sonido, etc... que nos permitirá simular:

- *man qemu-system-x86_64*
- *qemu-system-x86_64 -cpu ?*
- *qemu-system-x86_64 -soundhw ?*
- ...

### Creación *GUI* de una máquina virtual (virt-manager)

Para poder crear máquinas *qemu* con la *GUI* del *virt-manager* tendremos que instalar:

- *virt-manager*: Aplicación gráfica que nos permitirá gestionar las máquinas.
- *libvirtd*: Servicio que controlará el hipervisor KVM/qemu de nuestra máquina

Deberíamos ya tenerlos instalados anteriormente.

1. **Abrimos el virt-manager**: Nos pedirá la clave de root puesto que el servicio libvirtd corre bajo el usuario root. Si no la sabemos siempre podemos ejecutarlo con 'sudo virt-manager' y poner nuestra clave (tenemos que tener permisos, claro). Si nos dice que no puede abrir el 'display' tenemos que ejecutar previamente a la cuenta de nuestro usuario el comando: *xhost +*

 Una vez abierto, si todo ha ido bien, veremos nuestro hipervisor KVM/qemu conectado.

 **Crear nueva máquina**: 

 1. Elegir como queremos instalar el sistema operativo: Desde ISO, por red PXE o usando una imagen de disco (un fichero). Elegiremos esta última opción para crear la máquina del pinball anterior, ahora de manera gráfica

 2. Elegir almacenamiento y sistema operativo: Al clicar a navegar (browse) podremos bucar donde se encuentra el disco *freedos2016.qcow2*. Como que por defecto usa como directorio por los discos /var/lib/libvirt/images tendremos que añadir una nueva reserva (pool) con el signo + que hay abajo a la izquierda. Seleccionaremos el directorio donde tenemos nuestro disco (ex: /hombre/usuario/Bajadas) y le daremos un nombre a este directorio. Ahora que ya lo tenemos veremos a la parte derecha el disco que seleccionaremos.

    Al tipo de sistema operativo seleccionaremos 'Otros' y a versión 'MS-DOS 6.22'

 3. Ajustes de memoria y procesador: Ajustáis según convenga. Para nuestra máquina con 2MB de RAM es suficiente (pensad que es de una época donde esto tenían los ordenadores) y evidentemente no sabrá que hacer de más de un procesador.

 4. Finalizamos: Le ponemos nombre. Aquí también podríamos personalizar el hardware que queremos simular (y que, según el que hemos respondido ha seleccionado lo *virt-manager*), pero probaremos de iniciarla pulsando 'Finalizar'.

 Seguramente no iniciará (no se verá nada). Esto se debido a que el hardware seleccionado no se corresponde con el que este sistema operativo es capaz de detectar y espera encontrarse. Concretamente tendréis que parar la máquina e ir a la configuración de hardware (botón bombilla). Allí veréis que la tarjeta de audio que se ha seleccionado es ICH6 cuando, si recordáis, antes al arrancarla con *CLI* hemos visto que esperaba encontrarse una *SoundBlaster16 (sb16)*. Cambiadla y ya la podréis arrancar.

 El otro problema que os podéis encontrar es que ahora vaya mucho más rápida. Seguramente es por el tipo de procesador que le está simulando. Tendremos que jugar con los diferentes modelos para ajustarlo.

### Gestión de máquinas virtuales en *CLI (utilidades)

Las máquinas virtuales que creáis con el *virt-manager* en realidad son definiciones en un fichero XML que gestiona el servicio *libvirt. A este servicio también podemos acceder vía *CLI con el comando *virsh.

- **virsh list**: Mostrará las máquinas arrancadas. Con el parámetro **--all** nos mostrará todas, también los apagones.
- **virsh start <id | nombre>**: Indicando lo *nºid de máquina o su nombre la podremos arrancar.
- **virsh destroy <id | nombre>**: Para pararla
- **virsh undefine <id | nombre>**: Con este comando eliminaremos la máquina (no los discos que tenga)
- **virsh dumpxml <id | nombre>**: Ente volcará por pantalla la definición *xml* de la máquina. Tomaos un momento al mirar por sobre el contenido. veréis definiciones de memoria, cpu, audio... Podemos hacernos una copia de seguridad de la configuración de hardware de la máquina virtual si lo reenviamos a un fichero, por ejemplo añadiendo **> nombre.xml**
- **virsh define <nombre.xml>**: Nos creará una nueva máquina virtual con el hardware que haya definido al fichero nombre.*xml
- **virsh domdisplay <id | nombre>**: Nos mostrará la *URI de *conexio al visor. Con la orden **virt-viewer <URI>** que nos ha dado se nos abrirá el visor a la máquina virtual.

Investigáis otras opciones del *virsh* con su manual (man).

### Gestión de discos virtuales (qemu-img)

Podemos gestionar los discos con la orden *qemu-*img:

- **qemu-img info <disc.qcow2>**: Nos proporcionará información del disco como el *tamany real, *tamany virtual, formado... El tamaño virtual es el máximo permitido, el real es el que realmente está ocupado por el sistema operativo que hay dentro.
- **qemu-img create -f <format> <disc.qcow2> <tamany>**: Con este comando podemos crear un fichero llamado *disco.qcow2* que tenga un determinado formato y tamaño:
 - **formato**: qcow2, raw, img...
 - **tamaño**: En formato 20G por ejemplo indicaría que lo queremos de 20GB virtuales máximos.

En el manual del qemu-img encontraremos los diferentes formatos admitidos:

    Supported formats: blkdebug blklogwrites blkverify bochs cloop compress copy-before-write copy-on-read dmg file ftp ftps gluster host_cdrom host_device http https iscsi iser luks nbd null-aio null-co nvme parallels preallocate qcow qcow2 qed quorum raw rbd replication ssh throttle vdi vhdx vmdk vpc vvfat

### Interfaces virtuales (virtio)

Tanto los discos como la red pueden hacer uso de **hardware simulado** (hardware emulation). Por ejemplo conocemos que por los discos existen las interfaces IDE, SATA, SCSII... y por la red podemos simular diferentes tipos de tarjetas como e2000, rtl8931... 

Simular esta interfaz supone un coste computacional elevado. Es decir, que el rendimiento que obtendremos será bastante peor respecto al que obtendríamos en los mismos discos y redes en el hardware real. Es por eso que existen controladores paravirtualizados llamados **virtio** que permite lograr prácticamente los mismos rendimientos que si no estuviéramos virtualizando el sistema. 

Evidentemente hace falta que el sistema operativo que usamos disponga de los controladores (drivers) apropiados para este tipo de interfaz. Podéis encontrar más información a [https://spice-space.org](https://spice-space.org).

En esta presentación, en la página 20, podemos ver los diferentes modos en los que podemos usar un dispositivo dentro del *guest*: [https://www.usenix.org/sites/default/files/conference/protected-files/srecon20americas_slides_krosnov.pdf](https://www.usenix.org/sites/default/files/conference/protected-files/srecon20americas_slides_krosnov.pdf)

### Tipos de redes

Las redes pueden ser de diferentes tipos. Cada una nos proporcionará un tipo de *conexió diferente que nos puede ser útil en diferentes escenarios:

- **NAT**: Por defecto a las máquinas nos creará una red de este tipo. Se trata de una red con un rango de direcciones que gestionará el *libvirt* (mediante un servicio llamado dnsmasq) y que permitirá a la máquina comunicarse con otros ordenadores externos pero no permitirá que otros ordenadores se comuniquen con nuestra máquina virtual. La configuración es similar a la que tenemos a los *routers de casa..
- **ENCAMINADA (routed)**: En esta configuración la interfaz de red de la máquina virtual se conectada directamente a la interfaz real del ordenador. Por lo tanto su configuración dependerá de la red real, tal y como pasaría con cualquier ordenador en aquella red. Al *virt-manager* la veréis como **dispositivo de anfitrión**.
- **AISLADA (isolated)**: Las máquinas virtuales conectadas en una red aislada solo se podrán ver entre sí y con la máquina huésped. Nunca podrán salir hacia la red real. Al *virt-manager* habrá que ir a las propiedades de la hipervisor para crear redes aisladas que después podremos conectar a nuestras máquinas virtuales.

Más información: [https://wiki.libvirt.org/Networking.html](https://wiki.libvirt.org/Networking.html)